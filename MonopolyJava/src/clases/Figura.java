
package clases;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * Clase que permite elegir una figura al jugador
 * @author Darashc
 */
public class Figura {
	private String nombre; // Nombre de la figura
    private BufferedImage figura; // Imagen de la figura

    /**
     * Constructor de figura
     * @param figura Imagen de la figura 
     */
    public Figura(String nombre, BufferedImage figura) {
    	this.nombre = nombre;
        this.figura = figura;
    }

    /**
     * Getter de figura
     * @return figura image de figura
     */
    public BufferedImage getFigura() {
        return figura;
    }
    
    /**
     * Setter de figura
     * @param figura imagen de figura
     */
    public void setFigura(BufferedImage figura) {
        this.figura = figura;
    }

    /**
     * Getter de nombre
     * @return Nombre de la figura
     */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Setter de nombre
	 * @param nombre Nombre de la figura
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * Funci�n que inicializa todas las figuras del juego, para luego ser elegidas por el usuario
	 * @return Array de figuras inicializadas
	 */
	public static Figura[] inicializarFiguras() {
		Figura[] figuras=new Figura[8];
		try {
			figuras[0]=new Figura("Barco" ,ImageIO.read(new File("./imagenes/Figuras/Barco.png")));
			figuras[1]=new Figura("Caballo", ImageIO.read(new File("./imagenes/Figuras/Caballo.png")));
			figuras[2]=new Figura("Coche", ImageIO.read(new File("./imagenes/Figuras/Coche.png")));
			figuras[3]=new Figura("Guerrero", ImageIO.read(new File("./imagenes/Figuras/Guerrero.png")));
			figuras[4]=new Figura("Zapato", ImageIO.read(new File("./imagenes/Figuras/Zapato.png")));
			figuras[5]=new Figura("Gorra" ,ImageIO.read(new File("./imagenes/Figuras/Gorra.png")));
			figuras[6]=new Figura("Carretilla" ,ImageIO.read(new File("./imagenes/Figuras/Carretilla.png")));
			figuras[7]=new Figura("Dedal" ,ImageIO.read(new File("./imagenes/Figuras/Dedal.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return figuras;
	}
    
    
    // Fin Getter y Setter de la figura
    
}
