
package clases;

import java.util.ArrayList;
import java.util.Random;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import ventanas.PantallaPartida;

/**
 * Clase que simula un jugador
 * @author Darashc
 */
public class Jugador {
    private String nombre; // Nombre del jugador
    private int posicion; // Posicion del tablero
    private Figura figuraSeleccionada; // Figura seleccionada del jugador
    private int dineroBalance; // Dinero que tiene el jugador
    private ArrayList<Propiedad> propiedadesCompradas; // Propiedades que ha comprado el jugador
    private ArrayList<Propiedad> propiedadesHipotecables; // Propiedades que el jugador puede hipotecar
    private ArrayList<Propiedad> propiedadesRedimibles; // Propiedades que el jugador puede redimir
    private boolean estaBancarrota; // Comprueba si est� bancarrota
    private boolean estaEnCarcel; // Comprueba si est� en la carcel
    private int nCartasSalirCarcel; // Comprueba si tiene la carta "Salir de Carcel Gratis"
    
    /**
     * Constructor de la clase Jugador, al iniciar una partida
     * Al inicio de una partida, ni esta bancarrota, ni esta en la carcel, ni tiene propiedades compradas, ni tiene cartas de "Salir de la carcel gratis"
     * Por ello, se excluye del constructor
     * @param nombre Nombre del jugador
     * @param figuraSeleccionada Figura seleccionada del jugador
     */
    public Jugador(String nombre, Figura figuraSeleccionada) {
        this.nombre = nombre;
        this.posicion = 0;
        this.figuraSeleccionada = figuraSeleccionada;
        this.dineroBalance = 2000;
        this.propiedadesCompradas = new ArrayList<Propiedad>();
        this.propiedadesHipotecables = new ArrayList<Propiedad>();
        this.propiedadesRedimibles = new ArrayList<Propiedad>();
        this.estaBancarrota = false;
        this.estaEnCarcel = false;
        this.nCartasSalirCarcel = 0;
    }

    // Getters y setters de jugador
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Figura getFiguraSeleccionada() {
        return figuraSeleccionada;
    }

    public void setFiguraSeleccionada(Figura figuraSeleccionada) {
        this.figuraSeleccionada = figuraSeleccionada;
    }

    public int getDineroBalance() {
        return dineroBalance;
    }

    public void setDineroBalance(int l) {
        this.dineroBalance = l;
    }

    public ArrayList<Propiedad> getPropiedadesCompradas() {
		return propiedadesCompradas;
	}

	public void setPropiedadesCompradas(ArrayList<Propiedad> propiedadesCompradas) {
		this.propiedadesCompradas = propiedadesCompradas;
	}

	public boolean isEstaBancarrota() {
        return estaBancarrota;
    }

    public void setEstaBancarrota(boolean estaBancarrota) {
        this.estaBancarrota = estaBancarrota;
    }

    public boolean isEstaEnCarcel() {
        return estaEnCarcel;
    }

    public void setEstaEnCarcel(boolean estaEnCarcel) {
        this.estaEnCarcel = estaEnCarcel;
    }

    public int getnCartasSalirCarcel() {
        return nCartasSalirCarcel;
    }

    public void setnCartasSalirCarcel(int nCartasSalirCarcel) {
        this.nCartasSalirCarcel = nCartasSalirCarcel;
    }

	public int getPosicion() {
		return posicion;
	}

	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}
    
    

	public ArrayList<Propiedad> getPropiedadesHipotecables() {
		return propiedadesHipotecables;
	}

	public void setPropiedadesHipotecables(ArrayList<Propiedad> propiedadesHipotecables) {
		this.propiedadesHipotecables = propiedadesHipotecables;
	}

	public ArrayList<Propiedad> getPropiedadesRedimibles() {
		return propiedadesRedimibles;
	}

	public void setPropiedadesRedimibles(ArrayList<Propiedad> propiedadesRedimibles) {
		this.propiedadesRedimibles = propiedadesRedimibles;
	}
	// Fin getters y setters

	/**
	 * Funci�n que tira dados
	 * @return Entero entre 1 y 6 (Random)
	 */
	public int tirarDados() {
    	Random r=new Random();
    	return r.nextInt(6)+1;
    }
	
	/**
	 * Funcion que permite redimir/deshipotecar la propiedad de un usuario
	 */
	public void funcionRedimir() {
    	String accion="";
		if (this.getPropiedadesCompradas().size()==0||this.getPropiedadesRedimibles().size()==0) {
			JOptionPane.showMessageDialog(null, "No tienes ning�na propiedad comprada o que pueda ser redimible", "Redimir", JOptionPane.INFORMATION_MESSAGE);
		} else {
	        String[] options = new String[this.getPropiedadesRedimibles().size()];
	        for (int i=0; i<options.length; i++) {
	        	options[i]=this.getPropiedadesRedimibles().get(i).getNombre()+" - Precio: "+this.getPropiedadesRedimibles().get(i).getPrecioRedimir()+"�";
	        }
	        JComboBox comboBoxRedimir=new JComboBox(options);
	        
	        if (this.getNombre().contains("IA")) {
	        	Random r=new Random();
	        	int randRedimir=r.nextInt(this.getPropiedadesRedimibles().size());
	        	this.getPropiedadesRedimibles().get(randRedimir).setEstaHipotecado(false);
	        	this.getPropiedadesHipotecables().add(this.getPropiedadesRedimibles().get(randRedimir));
	        	this.setDineroBalance(this.getDineroBalance()-this.getPropiedadesRedimibles().get(randRedimir).getPrecioRedimir());
	        	accion=this.getNombre()+" ha redimido "+this.getPropiedadesRedimibles().get(randRedimir).getNombre()+" por "+this.getPropiedadesRedimibles().get(comboBoxRedimir.getSelectedIndex()).getPrecioRedimir()+"�";
	        	JOptionPane.showMessageDialog(null, accion, "Hipoteca", JOptionPane.INFORMATION_MESSAGE);
	        	PantallaPartida.resumenPartido("Turno "+PantallaPartida.nTurno+" - "+accion+"\n");
	        	this.getPropiedadesRedimibles().remove(randRedimir);
	        } else {
	        	int x=JOptionPane.showConfirmDialog(null, comboBoxRedimir, "Seleccione una propiedad a redimir", JOptionPane.DEFAULT_OPTION);
		        if (x==0) {
		        	this.getPropiedadesRedimibles().get(comboBoxRedimir.getSelectedIndex()).setEstaHipotecado(false);
		        	this.getPropiedadesHipotecables().add(this.getPropiedadesRedimibles().get(comboBoxRedimir.getSelectedIndex()));
		        	this.setDineroBalance(this.getDineroBalance()-this.getPropiedadesRedimibles().get(comboBoxRedimir.getSelectedIndex()).getPrecioRedimir());
		        	accion=this.getNombre()+" ha redimido "+this.getPropiedadesRedimibles().get(comboBoxRedimir.getSelectedIndex()).getNombre()+" por "+this.getPropiedadesRedimibles().get(comboBoxRedimir.getSelectedIndex()).getPrecioRedimir()+"�";
		        	JOptionPane.showMessageDialog(null, accion, "Hipoteca", JOptionPane.INFORMATION_MESSAGE);
		        	PantallaPartida.resumenPartido("Turno "+PantallaPartida.nTurno+" - "+accion+"\n");
		        	this.getPropiedadesRedimibles().remove(comboBoxRedimir.getSelectedIndex());
		        }
	        }
		}
    }
	
	/**
	 * Funcion que permite hipotecar la propiedad de un usuario
	 */
	public void funcionHipotecar() {
    	String accion="";
		if (this.getPropiedadesCompradas().size()==0||this.getPropiedadesHipotecables().size()==0) {
			JOptionPane.showMessageDialog(null, "No tienes ning�na propiedad comprada o que pueda ser hipotecable", "Hipoteca", JOptionPane.INFORMATION_MESSAGE);
		} else {
	        String[] options = new String[this.getPropiedadesHipotecables().size()];
	        for (int i=0; i<options.length; i++) {
	        	options[i]=this.getPropiedadesHipotecables().get(i).getNombre()+" - Precio: "+this.getPropiedadesHipotecables().get(i).getPrecioHipoteca()+"�";
	        }
	        JComboBox comboBoxHipotecar=new JComboBox(options);
	        
	        if (this.getNombre().contains("IA")) {
	        	Random r=new Random();
	        	int randHipotecar=r.nextInt(this.getPropiedadesHipotecables().size());
	        	this.getPropiedadesHipotecables().get(randHipotecar).setEstaHipotecado(true);
	        	this.getPropiedadesRedimibles().add(this.getPropiedadesHipotecables().get(randHipotecar));
	        	this.getPropiedadesHipotecables().remove(randHipotecar);
	        	this.setDineroBalance(this.getDineroBalance()+this.getPropiedadesCompradas().get(randHipotecar).getPrecioHipoteca());
	        	accion=this.getNombre()+" ha hipotecado "+this.getPropiedadesCompradas().get(randHipotecar).getNombre()+" por "+this.getPropiedadesCompradas().get(comboBoxHipotecar.getSelectedIndex()).getPrecioHipoteca()+"�";
	        	JOptionPane.showMessageDialog(null, accion, "Hipoteca", JOptionPane.INFORMATION_MESSAGE);
	        	PantallaPartida.resumenPartido("Turno "+PantallaPartida.nTurno+" - "+accion+"\n");
	        } else {
	        	int x=JOptionPane.showConfirmDialog(null, comboBoxHipotecar, "Seleccione una propiedad a hipotecar", JOptionPane.DEFAULT_OPTION);
		        if (x==0) {
		        	this.getPropiedadesHipotecables().get(comboBoxHipotecar.getSelectedIndex()).setEstaHipotecado(true);
		        	this.getPropiedadesRedimibles().add(this.getPropiedadesHipotecables().get(comboBoxHipotecar.getSelectedIndex()));
		        	this.getPropiedadesHipotecables().remove(comboBoxHipotecar.getSelectedIndex());
		        	this.setDineroBalance(this.getDineroBalance()+this.getPropiedadesCompradas().get(comboBoxHipotecar.getSelectedIndex()).getPrecioHipoteca());
		        	accion=this.getNombre()+" ha hipotecado "+this.getPropiedadesCompradas().get(comboBoxHipotecar.getSelectedIndex()).getNombre()+" por "+this.getPropiedadesCompradas().get(comboBoxHipotecar.getSelectedIndex()).getPrecioHipoteca()+"�";
		        	JOptionPane.showMessageDialog(null, accion, "Hipoteca", JOptionPane.INFORMATION_MESSAGE);
		        	PantallaPartida.resumenPartido("Turno "+PantallaPartida.nTurno+" - "+accion+"\n");
		        }
	        }
		}
    }
}
