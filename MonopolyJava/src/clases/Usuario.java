package clases;

import java.time.LocalDateTime;

import excepciones.EmailIncorrectoException;

public class Usuario {
	private String nombre; // Nombre del usuario
	private String email; // Email del usuario
	private String contrase�a; // Contrase�a del usuario
	private int nPartidas; // N� Partidas disputadas del usuario
	private int nVictorias; // N� Victorias del usuario
	private LocalDateTime lastConnection; // Ultima conexion del usuario

	/**
	 * Constructor de usuario, utilizado para recoger todos los datos de la base de datos
	 * @param nombre Nombre
	 * @param email Email
	 * @param contrase�a Contrase�a
	 * @param nPartidas N� Partidas
	 * @param nVictorias N� Victorias
	 * @param lastConnection Ultima conexi�n
	 */
	public Usuario(String nombre, String email, String contrase�a, int nPartidas, int nVictorias, LocalDateTime lastConnection) {
		super();
		this.nombre = nombre;
		this.contrase�a = contrase�a;
		this.email = email;
		this.nPartidas = nPartidas;
		this.nVictorias = nVictorias;
		this.lastConnection = lastConnection;
	}

	/**
	 * Constructor de usuario, utilizado para el registro
	 * @param nombre Nombre
	 * @param email Email
	 * @param contrase�a Contrase�a
	 * @throws EmailIncorrectoException Lanza error en caso de introducir el email erroneo
	 */
	public Usuario(String nombre, String email, String contrase�a) throws EmailIncorrectoException {
		super();
		this.nombre = nombre;
		this.setEmail(email);
		this.contrase�a = contrase�a;
		this.nPartidas = 0;
		this.nVictorias = 0;
		this.lastConnection = null;
	}


	// Getters y Setters de usuario
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getContrase�a() {
		return contrase�a;
	}

	public void setContrase�a(String contrase�a) {
		this.contrase�a = contrase�a;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) throws EmailIncorrectoException {
		if (email.contains("@")) {
			this.email = email;
		} else {
			throw new EmailIncorrectoException ("Ha introducido un email incorrecto. Por favor, h�galo");
		}
	}

	public int getnPartidas() {
		return nPartidas;
	}

	public void setnPartidas(int nPartidas) {
		this.nPartidas = nPartidas;
	}

	public LocalDateTime getLastConnection() {
		return lastConnection;
	}

	public void setLastConnection(String lastConnection) {
		this.lastConnection = LocalDateTime.parse(lastConnection);
	}

	public int getnVictorias() {
		return nVictorias;
	}

	public void setnVictorias(int nVictorias) {
		this.nVictorias = nVictorias;
	}
	// Fin getters y setters de usuario
	
	/**
	 * Funci�n que retorna todos los datos del usuario, cogido de la base de datos del juego
	 * @return Todos los datos del usuario en forma de string
	 */
	public String datosUsuario() {
		String ret="";
		
		ret="<html>Nombre: "+this.nombre+"<br>"
				+ "Email: "+this.email+"<br>"
				+ "N� Partidas jugadas: "+this.nPartidas+"<br>"
				+ "N� Victorias: "+this.nVictorias+"<br>"
				+ "�ltima conexi�n: <br>"+(this.lastConnection==null?"null":"Fecha - "+this.lastConnection.toLocalDate().toString()+"<br>Hora - "+this.lastConnection.toLocalTime().toString())+"</html>";
		return ret;
	}
}
