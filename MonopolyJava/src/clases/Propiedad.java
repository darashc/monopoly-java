
package clases;

/**
 * Clase que simula una propiedad
 * @author Darashc
 */
public class Propiedad {
	private Casilla casilla; // Casilla en la que se encuentra
	private String nombre; // Nombre de la propiedad
    private int valor; // Valor de la propiedad
    private int precioAlquiler; // Precio de renta de la propiedad (Influye el Nº casas)
    private int precioHipoteca; // Precio de hipoteca de la propiedad
    private boolean estaHipotecado; // Boolean que comprueba si esta hipotecado
    private int precioRedimir; // Precio al deshipotecar la propiedad
    private int nCasas; // Numero de Casas que tiene la propiedad (Si nCasas=5, entonces se coge el precio del Hotel)
    private int precioConstruir; // Precio al construir una casa en dicha propiedad
    private int precioVender; // Precio al vender una casa en dicha propiedad
    private Jugador due�o; // Due�o de la propiedad

    /**
     * Constructor de propiedad, en caso de que la propiedad sea una ciudad
     * @param posicion casilla en la que se encuentra
     * @param valor valor de la propiedad
     * @param dueno dueño de la propiedad
     * @param precioAlquiler precio de alquiler de la propiedad
     * @param precioHipoteca precio de hipoteca de la pripedad
     * @param estaHipotecado boolean que comprueba si esta hipotecado o no
     * @param precioRedimir precio al deshipotecar la propiedad
     * @param nCasa Nº casas que tiene la propiedad
     * @param precioConstruir precio al construir una casa en dicha propiedad
     * @param precioVender precio al vender una casa en dicha propiedad
     */
    public Propiedad(Casilla posicion, String nombre, int valor, int precioAlquiler, int precioHipoteca, int precioRedimir, int precioConstruir, int precioVender, Jugador d) {
        this.casilla = posicion;
        this.nombre = nombre;
        this.valor = valor;
        this.precioAlquiler = precioAlquiler;
        this.precioHipoteca = precioHipoteca;
        this.estaHipotecado = false;
        this.precioRedimir = precioRedimir;
        this.nCasas = 0;
        this.precioConstruir = precioConstruir;
        this.precioVender = precioVender;
        this.due�o=d;
    }
    
    /**
     * Constructor de propiedad, en caso de que la propiedad sea una estacion
     * No se puede construir ni vender casas en una estación, por ello un nuevo constructor
     * @param posicion casilla en la que se encuentra
     * @param valor valor de la propiedad
     * @param dueno dueño de la propiedad
     * @param precioAlquiler precio de alquiler de la propiedad
     * @param precioHipoteca precio de hipoteca de la pripedad
     * @param estaHipotecado boolean que comprueba si esta hipotecado o no
     * @param precioRedimir precio al deshipotecar la propiedad
     */
    public Propiedad(Casilla posicion, String nombre, int valor, int precioAlquiler, int precioHipoteca, int precioRedimir, Jugador d) {
    	this.casilla = posicion;
    	this.nombre = nombre;
        this.valor = valor;
        this.precioAlquiler = precioAlquiler;
        this.precioHipoteca = precioHipoteca;
        this.estaHipotecado = false;
        this.precioRedimir = precioRedimir;
        this.due�o=d;
    }

    // Getters y Setters de la clase propiedad
    public Casilla getPosicion() {
        return casilla;
    }

    public void setPosicion(Casilla posicion) {
        this.casilla = posicion;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public int getPrecioAlquiler() {
        return precioAlquiler;
    }

    public void setPrecioAlquiler(int precioAlquiler) {
        this.precioAlquiler = precioAlquiler;
    }

    public int getPrecioHipoteca() {
        return precioHipoteca;
    }

    public void setPrecioHipoteca(int precioHipoteca) {
        this.precioHipoteca = precioHipoteca;
    }

    public boolean isEstaHipotecado() {
        return estaHipotecado;
    }

    public void setEstaHipotecado(boolean estaHipotecado) {
        this.estaHipotecado = estaHipotecado;
    }

    public int getPrecioRedimir() {
        return precioRedimir;
    }

    public void setPrecioRedimir(int precioRedimir) {
        this.precioRedimir = precioRedimir;
    }

    public int getnCasas() {
        return nCasas;
    }

    public void setnCasas(int nCasas) {
        this.nCasas = nCasas;
    }

    public int getPrecioConstruir() {
        return precioConstruir;
    }

    public void setPrecioConstruir(int precioConstruir) {
        this.precioConstruir = precioConstruir;
    }

    public int getPrecioVender() {
        return precioVender;
    }

    public void setPrecioVender(int precioVender) {
        this.precioVender = precioVender;
    }

	public Jugador getDue�o() {
		return due�o;
	}

	public void setDue�o(Jugador due�o) {
		this.due�o = due�o;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
    // Fin Getters y Setters de la clase propiedad
    
    
}
