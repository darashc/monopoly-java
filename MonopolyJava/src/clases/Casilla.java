
package clases;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import constantes.Constantes;
import ventanas.PantallaPartida;

import java.awt.BorderLayout;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 * Clase que simula una casilla de monopoly
 * @author Darashc
 */
public class Casilla extends JPanel{
    private TipoCasilla tipo; // Tipo de casilla
    private Propiedad propiedad;
    private JLabel labelImagen;
    private Connection bd;
	private Statement casillaStatement;
    
    /**
     * Enum que indica que tipo de casilla es
     */
    public enum TipoCasilla {
        CIUDAD,
        ESTACION,
        SUERTE,
        ARCA_COMUNAL,
        TASAS,
        NO_PARKING,
        VISITA_CARCEL,
        SPA,
        GO;
    }

    /**
     * Constructor de casilla
     * @param imagen Imagen de la casilla
     * @param tipo Tipo de Casilla
     */
    public Casilla(BufferedImage imagen, TipoCasilla tipo) {
        this.tipo=tipo;
        labelImagen = new JLabel("");
        labelImagen.setIcon(new ImageIcon(imagen));
		setLayout(new BorderLayout(0, 0));
		add(labelImagen, BorderLayout.CENTER);
    }
    
    /**
     * Constructor de casilla con propiedad
     * @param imagen Imagen de la casilla
     * @param tipo Tipo de casilla
     * @param p Propiedad de la casilla
     */
    public Casilla(BufferedImage imagen, TipoCasilla tipo, Propiedad p) {
        this.tipo=tipo;
        this.propiedad=p;
        labelImagen = new JLabel("");
        labelImagen.setIcon(new ImageIcon(imagen));
		setLayout(new BorderLayout(0, 0));
		add(labelImagen, BorderLayout.CENTER);
    }
    
    /**
     * Getter de la imagen de casilla
     * @return imagen de tipo ImageIcon
     */
    public BufferedImage getImagen() {
    	return toBufferedImage(((ImageIcon)labelImagen.getIcon()).getImage());
    }
    
    /**
     * Funcion que convierte una imagen en BufferedImage
     * @param img Imagen de tipo image
     * @return Imagen de tipo BufferedImage
     */
    private  static BufferedImage toBufferedImage(Image img)
    {
        if (img instanceof BufferedImage)
        {
            return (BufferedImage) img;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }
    
    /**
     * Setter de la imagen de casilla
     * @param imagen imagen de tipo bufferedImage
     */
    public void setImagen(BufferedImage imagen) {
    	 labelImagen.setIcon(new ImageIcon(imagen));
    }
    
    /**
     * Getter de Tipo Casilla
     * @return Tipo Casilla
     */
    public TipoCasilla getTipo() {
        return tipo;
    }

    /**
     * Setter de Tipo Casilla
     * @param tipo Tipo Casilla
     */
    public void setTipo(TipoCasilla tipo) {
        this.tipo = tipo;
    }
    
    /**
     * Getter de propiedad
     * @return propiedad
     */
    public Propiedad getPropiedad() {
		return propiedad;
	}
    
    /**
     * Setter de propiedad
     * @param propiedad propiedad
     */
	public void setPropiedad(Propiedad propiedad) {
		this.propiedad = propiedad;
	}
    
    /**
     * Funcion que inicializa las casillas
     * @return Arrays de casillas inicializadas
     */
    public static Casilla[] inicializarCasillas() {
		Casilla[] c=new Casilla[40];
		try {
			c[0]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/0.png")), TipoCasilla.GO);
			c[1]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/1.png")), TipoCasilla.CIUDAD, null);
			c[2]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/2.png")), TipoCasilla.ARCA_COMUNAL);
			c[3]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/3.png")), TipoCasilla.CIUDAD, null);
			c[4]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/4.png")), TipoCasilla.TASAS);
			c[5]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/5.png")), TipoCasilla.ESTACION, null);
			c[6]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/6.png")), TipoCasilla.CIUDAD, null);
			c[7]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/7.png")), TipoCasilla.CIUDAD, null);
			c[8]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/8.png")), TipoCasilla.SUERTE);
			c[9]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/9.png")), TipoCasilla.CIUDAD, null);
			c[10]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/10.png")), TipoCasilla.VISITA_CARCEL);
			c[11]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/11.png")), TipoCasilla.CIUDAD, null);
			c[12]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/12.png")), TipoCasilla.CIUDAD, null);
			c[13]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/13.png")), TipoCasilla.CIUDAD, null);
			c[14]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/14.png")), TipoCasilla.CIUDAD, null);
			c[15]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/15.png")), TipoCasilla.ESTACION, null);
			c[16]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/16.png")), TipoCasilla.ARCA_COMUNAL);
			c[17]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/17.png")), TipoCasilla.CIUDAD, null);
			c[18]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/18.png")), TipoCasilla.CIUDAD, null);
			c[19]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/19.png")), TipoCasilla.CIUDAD, null);
			c[20]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/20.png")), TipoCasilla.SPA);
			c[21]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/21.png")), TipoCasilla.CIUDAD, null);
			c[22]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/22.png")), TipoCasilla.CIUDAD, null);
			c[23]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/23.png")), TipoCasilla.SUERTE);
			c[24]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/24.png")), TipoCasilla.CIUDAD, null);
			c[25]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/25.png")), TipoCasilla.ESTACION, null);
			c[26]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/26.png")), TipoCasilla.CIUDAD, null);
			c[27]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/27.png")), TipoCasilla.CIUDAD, null);
			c[28]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/28.png")), TipoCasilla.CIUDAD, null);
			c[29]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/29.png")), TipoCasilla.CIUDAD, null);
			c[30]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/30.png")), TipoCasilla.NO_PARKING);
			c[31]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/31.png")), TipoCasilla.CIUDAD, null);
			c[32]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/32.png")), TipoCasilla.CIUDAD, null);
			c[33]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/33.png")), TipoCasilla.CIUDAD, null);
			c[34]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/34.png")), TipoCasilla.ESTACION, null);
			c[35]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/35.png")), TipoCasilla.ARCA_COMUNAL);
			c[36]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/36.png")), TipoCasilla.TASAS);
			c[37]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/37.png")), TipoCasilla.CIUDAD, null);
			c[38]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/38.png")), TipoCasilla.SUERTE);
			c[39]=new Casilla(ImageIO.read(new File("./imagenes/Tablero/39.png")), TipoCasilla.CIUDAD, null);
			
			return c;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return c;
	}
    
    /**
     * Funcion que simula el caso de Suerte de la casilla
     * @param j Jugador afectada
     * @param fig Figura del jugador
     * @param indx indice de jugador
     */
    public void casoSuerte(Jugador j, JLabel fig, int indx) {
    	Random r=new Random();
    	try {
			bd=DriverManager.getConnection(Constantes.bdNombre, Constantes.bdUsuario, Constantes.bdContra);
			casillaStatement=bd.createStatement();
			
			int randId=1+r.nextInt(11);
			
			ResultSet resSuerte=casillaStatement.executeQuery("SELECT descripcion"
					+ " FROM suerte"
					+ " WHERE id="+randId+"");
			
			if (resSuerte.next()) {
				JOptionPane.showMessageDialog(this.getParent(), resSuerte.getString("descripcion"));
				PantallaPartida.resumenPartido("Turno "+PantallaPartida.nTurno+" - "+j.getNombre()+", Suerte: "+resSuerte.getString("descripcion"));
				switch (randId) {
				case 1:
					this.remove(fig);
					j.setPosicion(39);
					PantallaPartida.mostrarFiguraEnTablero(j.getPosicion(), fig, indx);
					this.setVisible(false);
					this.getParent().repaint();
					this.setVisible(true);
					break;
				case 2:
					this.remove(fig);
					if (5-j.getPosicion()<=0) {
						j.setDineroBalance(j.getDineroBalance()+200);
						JOptionPane.showMessageDialog(this.getParent(), "Has pasado por la casilla GO, obtienes 200�");
					}
					j.setPosicion(5);
					PantallaPartida.mostrarFiguraEnTablero(j.getPosicion(), fig, indx);
					this.setVisible(false);
					this.getParent().repaint();
					this.setVisible(true);
					break;
				case 3:
					j.setDineroBalance(j.getDineroBalance()+150);
					break;
				case 4:
					this.remove(fig);
					if (13-j.getPosicion()<=0) {
						j.setDineroBalance(j.getDineroBalance()+200);
						JOptionPane.showMessageDialog(this.getParent(), "Has pasado por la casilla GO, obtienes 200�");
					}
					j.setPosicion(13);
					PantallaPartida.mostrarFiguraEnTablero(j.getPosicion(), fig, indx);
					this.setVisible(false);
					this.getParent().repaint();
					this.setVisible(true);
					break;
				case 5:
					this.remove(fig);
					j.setPosicion(10);
					PantallaPartida.mostrarFiguraEnTablero(j.getPosicion(), fig, indx);
					this.setVisible(false);
					this.getParent().repaint();
					this.setVisible(true);
					j.setEstaEnCarcel(true);
					break;
				case 6:
					this.remove(fig);
					j.setPosicion(0);
					PantallaPartida.mostrarFiguraEnTablero(j.getPosicion(), fig, indx);
					this.setVisible(false);
					this.getParent().repaint();
					this.setVisible(true);
					j.setDineroBalance(j.getDineroBalance()+200);
					break;
				case 7:
					j.setDineroBalance(j.getDineroBalance()+50);
					break;
				case 8:
					j.setnCartasSalirCarcel(j.getnCartasSalirCarcel()+1);
					break;
				case 9:
					this.remove(fig);
					if (24-j.getPosicion()<=0) {
						j.setDineroBalance(j.getDineroBalance()+200);
						JOptionPane.showMessageDialog(this.getParent(), "Has pasado por la casilla GO, obtienes 200�");
					}
					j.setPosicion(24);
					PantallaPartida.mostrarFiguraEnTablero(j.getPosicion(), fig, indx);
					this.setVisible(false);
					this.getParent().repaint();
					this.setVisible(true);
					break;
				case 10:
					this.remove(fig);
					j.setPosicion(j.getPosicion()-3);
					PantallaPartida.mostrarFiguraEnTablero(j.getPosicion(), fig, indx);
					this.setVisible(false);
					this.getParent().repaint();
					this.setVisible(true);
					break;
				case 11:
					j.setDineroBalance(j.getDineroBalance()-15);
					break;
				}
					
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this.getParent(), "Error");
			e.printStackTrace();
		}
    }
    
    /**
     * Funcion que simula un caso de Arca Comunal
     * @param j Jugador afectada
     * @param fig Figura del jugador
     * @param indx Indice del jugador
     */
    public void casoArcaComunal(Jugador j, JLabel fig, int indx) {
    	Random r=new Random();
    	try {
			bd=DriverManager.getConnection(Constantes.bdNombre, Constantes.bdUsuario, Constantes.bdContra);
			casillaStatement=bd.createStatement();
			
			int randId=1+r.nextInt(14);
			
			ResultSet resArcaComunal=casillaStatement.executeQuery("SELECT descripcion"
					+ " FROM arca_comunal"
					+ " WHERE id="+randId+"");
			
			if (resArcaComunal.next()) {
				JOptionPane.showMessageDialog(this.getParent(), resArcaComunal.getString("descripcion"));
				PantallaPartida.resumenPartido("Turno "+PantallaPartida.nTurno+" - "+j.getNombre()+", Suerte: "+resArcaComunal.getString("descripcion"));
				switch (randId) {
				case 1:
					j.setnCartasSalirCarcel(j.getnCartasSalirCarcel()+1);
					break;
				case 2:
					j.setDineroBalance(j.getDineroBalance()-50);
					break;
				case 3:
					j.setDineroBalance(j.getDineroBalance()+100);
					break;
				case 4:
					j.setDineroBalance(j.getDineroBalance()+100);
					break;
				case 5:
					j.setDineroBalance(j.getDineroBalance()-50);
					break;
				case 6:
					j.setDineroBalance(j.getDineroBalance()+20);
					break;
				case 7:
					j.setDineroBalance(j.getDineroBalance()-100);
					break;
				case 8:
					this.remove(fig);
					j.setPosicion(10);
					PantallaPartida.mostrarFiguraEnTablero(j.getPosicion(), fig, indx);
					this.setVisible(false);
					this.getParent().repaint();
					this.setVisible(true);
					j.setEstaEnCarcel(true);
					break;
				case 9:
					j.setDineroBalance(j.getDineroBalance()+25);
					break;
				case 10:
					j.setDineroBalance(j.getDineroBalance()+100);
					break;
				case 11:
					j.setDineroBalance(j.getDineroBalance()+50);
					break;
				case 12:
					j.setDineroBalance(j.getDineroBalance()+10);
					break;
				case 13:
					this.remove(fig);
					j.setPosicion(0);
					PantallaPartida.mostrarFiguraEnTablero(j.getPosicion(), fig, indx);
					this.setVisible(false);
					this.getParent().repaint();
					this.setVisible(true);
					j.setDineroBalance(j.getDineroBalance()+200);
					break;
				case 14:
					j.setDineroBalance(j.getDineroBalance()+200);
					break;
				}
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this.getParent(), "Error");
			e.printStackTrace();
		}
    }
    
    /**
     * Funcion que simula el caso de pagar tasas
     * @param pos Posicion de la casilla
     * @param j Jugador afectada
     */
    public void casoTasas(int pos, Jugador j) {
    	String accion="";
    	if (pos==4) {
    		accion="Deber�s cobrar el IVA. Pagar el 21% de tu cuenta bancaria";
    		JOptionPane.showMessageDialog(this.getParent(), accion, "Tasas", JOptionPane.INFORMATION_MESSAGE);
    		j.setDineroBalance((int) (j.getDineroBalance()*0.79));
    	} else if (pos==36) {
    		accion="Gastos mensuales. Pagar 100� de tu cuenta bancaria";
    		JOptionPane.showMessageDialog(this.getParent(), accion, "Tasas", JOptionPane.INFORMATION_MESSAGE);
    		j.setDineroBalance(j.getDineroBalance()-100);
    	}
    	PantallaPartida.resumenPartido("Turno "+PantallaPartida.nTurno+" - "+j.getNombre()+", Gastos: "+accion);
    }
    
    /**
     * Funci�n que simula el caso de una estaci�n
     * @param j Jugador afectado
     */
    public void casoEstacion(Jugador j) {
    	int opt;
    	String accion="";
    	if (this.getPropiedad()==null) { // Si propiedad no tiene due�o
    		if (j.getNombre().contains(String.valueOf("IA"))) { // Funci�n para la IA
    			try {
					bd=DriverManager.getConnection(Constantes.bdNombre, Constantes.bdUsuario, Constantes.bdContra);
					casillaStatement=bd.createStatement();
					
					ResultSet resEstacion=casillaStatement.executeQuery("SELECT *"
							+ " FROM estaciones"
							+ " WHERE posicion="+j.getPosicion()+"");
					
					if (resEstacion.next()) {
						if (j.getDineroBalance()>=resEstacion.getInt("valor")) {
							a�adirEstacion(resEstacion, j, resEstacion.getInt("valor"));
						} else if (j.getPropiedadesHipotecables().size()!=0) {
							do {
								j.funcionHipotecar();
							} while (j.getPropiedadesHipotecables().size()!=0&&j.getDineroBalance()<resEstacion.getInt("valor"));
							if (j.getDineroBalance()>=resEstacion.getInt("valor")) {
								a�adirEstacion(resEstacion, j, resEstacion.getInt("valor"));
							} else {
								funcionSubastar(TipoCasilla.ESTACION, resEstacion, resEstacion.getInt("valor"));
							}
						}
					}
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(this.getParent(), "Error en la base de datos");
					e.printStackTrace();
				} finally {
					try {
						casillaStatement.close();
						bd.close();
					} catch (SQLException e) {
						JOptionPane.showMessageDialog(this.getParent(), "Error en la base de datos");
						e.printStackTrace();
					}
				}
    		} else {
    			do { // Funci�n para el usuario
        			opt=JOptionPane.showOptionDialog(this.getParent(), "�Parece ser que est� propiedad est� disponible!\nElija lo que desea hacer", j.getNombre(), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, new String[] {"Comprar", "Subastar"}, null);
            		if (opt==0) {
            			try {
    						bd=DriverManager.getConnection(Constantes.bdNombre, Constantes.bdUsuario, Constantes.bdContra);
    						casillaStatement=bd.createStatement();
    						
    						ResultSet resEstacion=casillaStatement.executeQuery("SELECT *"
    								+ " FROM estaciones"
    								+ " WHERE posicion="+j.getPosicion()+"");
    						
    						if (resEstacion.next()) {
    							if (j.getDineroBalance()>=resEstacion.getInt("valor")) {
        							a�adirEstacion(resEstacion, j, resEstacion.getInt("valor"));
        						} else {
        							int opcion;
        							do {
        								opcion=JOptionPane.showOptionDialog(this.getParent(), "�oops! No tiene suficiente dinero para comprar esta propiedad\\nElija lo que desa hacer", j.getNombre(), JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, new String[] {"Subastar", "Hipotecar"}, null);
        								if (opcion==-1) {
        									JOptionPane.showMessageDialog(this.getParent(), "Por favor, elija una de las opciones", j.getNombre(), JOptionPane.ERROR_MESSAGE);
        								}
        							} while(opcion==-1);
        							if (opcion==0) {
        								funcionSubastar(TipoCasilla.ESTACION, resEstacion, resEstacion.getInt("valor"));
        							} else if (opcion==1) {
        								if (j.getPropiedadesHipotecables().size()==0) {
        									j.funcionHipotecar();
        								} else {
        									JOptionPane.showMessageDialog(this.getParent(), "No tienes ning�na propiedad comprada o que pueda ser hipotecable", "Hipoteca", JOptionPane.INFORMATION_MESSAGE);
        								}
        							}
        						}
    						}
    					} catch (SQLException e) {
    						JOptionPane.showMessageDialog(this.getParent(), "Error en la base de datos");
    						e.printStackTrace();
    					} finally {
    						try {
    							casillaStatement.close();
    							bd.close();
    						} catch (SQLException e) {
    							JOptionPane.showMessageDialog(this.getParent(), "Error en la base de datos");
    							e.printStackTrace();
    						}
    					}
            		} else if (opt==1) {
            			try {
							bd=DriverManager.getConnection(Constantes.bdNombre, Constantes.bdUsuario, Constantes.bdContra);
							casillaStatement=bd.createStatement();
							
							ResultSet resEstacion=casillaStatement.executeQuery("SELECT *"
									+ " FROM estaciones"
									+ " WHERE posicion="+j.getPosicion()+"");
							
							if (resEstacion.next()) {
								funcionSubastar(TipoCasilla.ESTACION, resEstacion, resEstacion.getInt("valor"));
							}
						} catch (SQLException e) {
							JOptionPane.showMessageDialog(this.getParent(), "Error en la base de datos");
    						e.printStackTrace();
						} finally {
    						try {
    							casillaStatement.close();
    							bd.close();
    						} catch (SQLException e) {
    							JOptionPane.showMessageDialog(this.getParent(), "Error en la base de datos");
    							e.printStackTrace();
    						}
    					}
            		}
        		} while (opt==-1);
    		}
    	} else { // Si propiedad es comprado por jugador
    		if (!j.getNombre().equals(this.propiedad.getDue�o().getNombre())) {
    			if (this.propiedad.isEstaHipotecado()) {
    				accion="Esta propiedad ("+this.propiedad.getNombre()+") est� hipotecada. No pagas alquiler al due�o.";
    				JOptionPane.showMessageDialog(this.getParent(), accion);
    				PantallaPartida.resumenPartido(accion+"\n");
    			} else {
    				accion=j.getNombre()+" debe pagar "+this.propiedad.getPrecioAlquiler()+"� al due�o de "+this.propiedad.getNombre()+", "+this.propiedad.getDue�o().getNombre();
    				JOptionPane.showMessageDialog(this.getParent(), accion);
            		j.setDineroBalance(j.getDineroBalance()-this.propiedad.getPrecioAlquiler());
            		this.propiedad.getDue�o().setDineroBalance(this.propiedad.getDue�o().getDineroBalance()+this.propiedad.getPrecioAlquiler());
            		PantallaPartida.resumenPartido("Turno "+PantallaPartida.nTurno+" - "+accion+"\n");
    			}
    		} else {
    			JOptionPane.showMessageDialog(this.getParent(), "Esta es su propia propiedad");
    		}
    	}
    }
    
    /**
     * Funcion que simula un caso para la ciudad
     * @param j Jugador afectada
     */
    public void casoCiudad(Jugador j) {
    	int opt;
    	String accion = "";
    	if (this.getPropiedad()==null) { // Si propiedad no tiene due�o
    		if (j.getNombre().contains(String.valueOf("IA"))) { // Funci�n para la IA
    			try {
					bd=DriverManager.getConnection(Constantes.bdNombre, Constantes.bdUsuario, Constantes.bdContra);
					casillaStatement=bd.createStatement();
					
					ResultSet resCiudad=casillaStatement.executeQuery("SELECT *"
							+ " FROM propiedades"
							+ " WHERE posicion="+j.getPosicion()+"");
					
					if (resCiudad.next()) {
						if (j.getDineroBalance()>=resCiudad.getInt("valor")) {
							a�adirCiudad(resCiudad, j, resCiudad.getInt("valor"));
						} else if (j.getPropiedadesHipotecables().size()!=0) {
							do {
								j.funcionHipotecar();
							} while (j.getPropiedadesHipotecables().size()!=0&&j.getDineroBalance()<resCiudad.getInt("valor"));
							if (j.getDineroBalance()>=resCiudad.getInt("valor")) {
								a�adirCiudad(resCiudad, j, resCiudad.getInt("valor"));
							} else {
								funcionSubastar(TipoCasilla.CIUDAD, resCiudad, resCiudad.getInt("valor"));
							}
						}
					}
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(this.getParent(), "Error en la base de datos");
					e.printStackTrace();
				} finally {
					try {
						casillaStatement.close();
						bd.close();
					} catch (SQLException e) {
						JOptionPane.showMessageDialog(this.getParent(), "Error en la base de datos");
						e.printStackTrace();
					}
				}
    		} else {
    			do { // Funci�n para usuario
        			opt=JOptionPane.showOptionDialog(this.getParent(), "�Parece ser que est� propiedad est� disponible!\nElija lo que desea hacer", j.getNombre(), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, new String[] {"Comprar", "Subastar"}, null);
            		if (opt==0) {
            			try {
    						bd=DriverManager.getConnection(Constantes.bdNombre, Constantes.bdUsuario, Constantes.bdContra);
    						casillaStatement=bd.createStatement();
    						
    						ResultSet resCiudad=casillaStatement.executeQuery("SELECT *"
    								+ " FROM propiedades"
    								+ " WHERE posicion="+j.getPosicion()+"");
    						
    						if (resCiudad.next()) {
    							if (j.getDineroBalance()>=resCiudad.getInt("valor")) {
    								a�adirCiudad(resCiudad, j, resCiudad.getInt("valor"));
    							} else {
    								int opcion;
        							do {
        								opcion=JOptionPane.showOptionDialog(this.getParent(), "�oops! No tiene suficiente dinero para comprar esta propiedad\\nElija lo que desa hacer", j.getNombre(), JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, new String[] {"Subastar", "Hipotecar"}, null);
        								if (opcion==-1) {
        									JOptionPane.showMessageDialog(this.getParent(), "Por favor, elija una de las opciones", j.getNombre(), JOptionPane.ERROR_MESSAGE);
        								}
        							} while(opcion==-1);
        							if (opcion==0) {
        								funcionSubastar(TipoCasilla.CIUDAD, resCiudad, resCiudad.getInt("valor"));
        							} else if (opcion==1) {
        								if (j.getPropiedadesHipotecables().size()==0) {
        									j.funcionHipotecar();
        								} else {
        									JOptionPane.showMessageDialog(this.getParent(), "No tienes ning�na propiedad comprada o que pueda ser hipotecable", "Hipoteca", JOptionPane.INFORMATION_MESSAGE);
        								}
        							}
        						}
    						}
    					} catch (SQLException e) {
    						JOptionPane.showMessageDialog(this.getParent(), "Error en la base de datos");
    						e.printStackTrace();
    					} finally {
    						try {
    							casillaStatement.close();
    							bd.close();
    						} catch (SQLException e) {
    							JOptionPane.showMessageDialog(this.getParent(), "Error en la base de datos");
    							e.printStackTrace();
    						}
    					}
            		} else if (opt==1) {
            			try {
							bd=DriverManager.getConnection(Constantes.bdNombre, Constantes.bdUsuario, Constantes.bdContra);
							casillaStatement=bd.createStatement();
							
							ResultSet resCiudad=casillaStatement.executeQuery("SELECT *"
									+ " FROM propiedades"
									+ " WHERE posicion="+j.getPosicion()+"");
							
							
							if (resCiudad.next()) {
								funcionSubastar(TipoCasilla.CIUDAD, resCiudad, resCiudad.getInt("valor"));
							}
						} catch (SQLException e) {
							JOptionPane.showMessageDialog(this.getParent(), "Error de base de datos", "Error", JOptionPane.ERROR_MESSAGE);
							e.printStackTrace();
						} finally {
    						try {
    							casillaStatement.close();
    							bd.close();
    						} catch (SQLException e) {
    							JOptionPane.showMessageDialog(this.getParent(), "Error en la base de datos");
    							e.printStackTrace();
    						}
    					}
            		}
        		} while (opt==-1);
    		}
    	} else { // Si propiedad esta comprado por Jugador
    		if (!j.getNombre().equals(this.propiedad.getDue�o().getNombre())) {
    			if (this.propiedad.isEstaHipotecado()) {
    				accion="Turno "+PantallaPartida.nTurno+" - Esta propiedad ("+this.propiedad.getNombre()+") est� hipotecada. No pagas alquiler al due�o.";
    				JOptionPane.showMessageDialog(this.getParent(), accion);
    				PantallaPartida.resumenPartido(accion+"\n");
    			} else {
    				accion=j.getNombre()+" debe pagar "+this.propiedad.getPrecioAlquiler()+"� al due�o de "+this.propiedad.getNombre()+", "+this.propiedad.getDue�o().getNombre();
    				JOptionPane.showMessageDialog(this.getParent(), accion);
            		j.setDineroBalance(j.getDineroBalance()-this.propiedad.getPrecioAlquiler());
            		this.propiedad.getDue�o().setDineroBalance(this.propiedad.getDue�o().getDineroBalance()+this.propiedad.getPrecioAlquiler());
            		PantallaPartida.resumenPartido("Turno "+PantallaPartida.nTurno+" - "+accion+"\n");
    			}
    		} else {
    			JOptionPane.showMessageDialog(this.getParent(), "Esta es su propia propiedad");
    		}
    	}
    }
    
    /**
     * Funci�n que a�ade la propiedad ciudad a Jugador
     * @param resCiudad ResultSet de ciudad
     * @param j Jugador afectado
     * @param precio Valor de la propiedad o subasta m�xima
     * @throws SQLException Error de base de datos
     */
    public void a�adirCiudad(ResultSet resCiudad, Jugador j, int precio) throws SQLException {
		String accion=j.getNombre()+" ha comprado "+resCiudad.getString("nombre")+" por "+precio+"�";
    	Propiedad p=new Propiedad(this, resCiudad.getString("nombre"), resCiudad.getInt("valor"), resCiudad.getInt("precioAlquiler"), resCiudad.getInt("precioHipoteca"), resCiudad.getInt("precioRedimir"), resCiudad.getInt("precioConstruir"), resCiudad.getInt("precioVender"), j);
		j.setDineroBalance(j.getDineroBalance()-precio);
		j.getPropiedadesCompradas().add(p);
		j.getPropiedadesHipotecables().add(p);
		this.setPropiedad(p);
		JOptionPane.showMessageDialog(this.getParent(), accion);
		PantallaPartida.resumenPartido("Turno "+PantallaPartida.nTurno+" - "+accion+"\n");
    }
    
    /**
     * Funci�n que a�ade la propiedad estacion a Jugador
     * @param resCiudad ResultSet de estacion
     * @param j Jugador afectado
     * @param precio Valor de la propiedad o subasta m�xima
     * @throws SQLException Error de base de datos
     */
    public void a�adirEstacion(ResultSet resEstacion, Jugador j, int precio) throws SQLException {
    	String accion=j.getNombre()+" ha comprado "+resEstacion.getString("nombre")+" por "+precio+"�";
    	Propiedad p=new Propiedad(this, resEstacion.getString("nombre"), resEstacion.getInt("valor"), resEstacion.getInt("precioAlquiler1"), resEstacion.getInt("precioHipoteca"), resEstacion.getInt("precioRedimir"), j);
		j.setDineroBalance(j.getDineroBalance()-precio);
		j.getPropiedadesCompradas().add(p);
		j.getPropiedadesHipotecables().add(p);
		this.setPropiedad(p);
		JOptionPane.showMessageDialog(this.getParent(), accion);
		PantallaPartida.resumenPartido("Turno "+PantallaPartida.nTurno+" - "+accion+"\n");
    }
    
    /**
     * Funci�n que simula la subasta de una propiedad
     * @param tp TipoCasilla (Estacion o Ciudad)
     * @param res ResultSet de la propiedad
     * @param valorPropiedad El valor de la propiedad
     * @throws SQLException Error de base de datos
     */
    public void funcionSubastar(TipoCasilla tp,ResultSet res, int valorPropiedad) throws SQLException {
    	String subastaString=null;
		Integer subastaInt=null;
    	do {
			subastaString=JOptionPane.showInputDialog("Escriba su subasta m�xima");
			if (subastaString==null) {
				JOptionPane.showMessageDialog(this.getParent(), "Por favor, escriba su subasta m�xima");
			} else if (!subastaString.matches("[0-9]+")) {
				JOptionPane.showMessageDialog(this.getParent(), "Por favor, SOLO escriba n�meros");
			} else if (Integer.parseInt(subastaString)>PantallaPartida.jugadores[0].getDineroBalance()){
				JOptionPane.showMessageDialog(this.getParent(), "No tiene suficiente dinero para subastar la cantidad introducida", "Subasta", JOptionPane.ERROR_MESSAGE);
			} else {
				subastaInt=Integer.parseInt(subastaString);
			}
		} while (subastaString==null||subastaInt==null);
		
		Random r=new Random();
		int[] cantidadApostada=new int[PantallaPartida.jugadores.length];
		int subastaMaxima=0;
		int indiceJugador=0;
		String texto="Subastas:\n";
		
		for (int i=0; i<PantallaPartida.jugadores.length; i++) {
			if (i==0) {
				cantidadApostada[0]=subastaInt;
				subastaMaxima=subastaInt;
			} else {
				if ((valorPropiedad+r.nextInt(valorPropiedad/2)<PantallaPartida.jugadores[i].getDineroBalance())) {
					cantidadApostada[i]=valorPropiedad+r.nextInt(valorPropiedad/2);
				} else {
					cantidadApostada[i]=PantallaPartida.jugadores[i].getDineroBalance();
				}
				if (cantidadApostada[i]>subastaMaxima) {
					indiceJugador=i;
					subastaMaxima=cantidadApostada[i];
				}
			}
			texto+=PantallaPartida.jugadores[i].getNombre()+" - "+cantidadApostada[i]+"�\n";
		}
		JOptionPane.showMessageDialog(this.getParent(), texto);
		if (tp==TipoCasilla.CIUDAD) {
			a�adirCiudad(res, PantallaPartida.jugadores[indiceJugador], subastaMaxima);
		} else if (tp==TipoCasilla.ESTACION) {
			a�adirEstacion(res, PantallaPartida.jugadores[indiceJugador], subastaMaxima);
		}
    }
    
    /**
     * Funcion que simula el caso de un jugador en la c�rcel
     * @param dado1 Dado 1
     * @param dado2 Dado 2
     * @param j Jugador afectada
     */
    public void casoCarcel(int dado1, int dado2, Jugador j) {
    	int opt;
		String accion;
		if (j.getNombre().contains("IA")) {
			if (j.getnCartasSalirCarcel()>0) {
				j.setEstaEnCarcel(false);
				accion=j.getNombre()+" sale de la c�rcel usando la carta 'SALGA DE LA CARCEL GRATIS'";
				j.setnCartasSalirCarcel(j.getnCartasSalirCarcel()-1);
				PantallaPartida.resumenPartido("Turno "+PantallaPartida.nTurno+" - "+accion+"\n");
				JOptionPane.showMessageDialog(this.getParent(), "Dado 1: "+dado1+"\nDado 2: "+dado2, "Tira "+j.getNombre(), JOptionPane.INFORMATION_MESSAGE);
			} else if (j.getDineroBalance()>=100) {
				j.setEstaEnCarcel(false);
				accion=j.getNombre()+" sale de la c�rcel pagando 100�";
				j.setDineroBalance(j.getDineroBalance()-100);
				JOptionPane.showMessageDialog(this.getParent(), accion);
				PantallaPartida.resumenPartido("Turno "+PantallaPartida.nTurno+" - "+accion+"\n");
				JOptionPane.showMessageDialog(this.getParent(), "Dado 1: "+dado1+"\nDado 2: "+dado2, "Tira "+j.getNombre(), JOptionPane.INFORMATION_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(this.getParent(), "Dado 1: "+dado1+"\nDado 2: "+dado2, "Tira "+j.getNombre(), JOptionPane.INFORMATION_MESSAGE);
				if (dado1==dado2) {
					j.setEstaEnCarcel(false);
					accion=j.getNombre()+" sale de la c�rcel tirando dobles";
					j.setEstaEnCarcel(false);
					JOptionPane.showMessageDialog(this.getParent(), accion);
					PantallaPartida.resumenPartido("Turno "+PantallaPartida.nTurno+" - "+accion+"\n");
				} else {
					JOptionPane.showMessageDialog(this.getParent(), "No le ha salido dobles. No puede salir de la c�rcel");
				}
			}
		} else {
			do {
				opt=JOptionPane.showOptionDialog(this.getParent(), "�Esta usted en la c�rcel!\nElija lo que desea hacer", j.getNombre(), JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, new String[] {"Dados", "Pagar 100�", "Usar Carta"}, null);
				if (opt==-1) {
					JOptionPane.showMessageDialog(this.getParent(), "Por favor, elija una de las opciones para la oportunidad de salir de la c�rcel", j.getNombre(), JOptionPane.ERROR_MESSAGE);
				}
			} while (opt==-1);
			if (opt==0) {
				JOptionPane.showMessageDialog(this.getParent(), "Dado 1: "+dado1+"\nDado 2: "+dado2, "Tira "+j.getNombre(), JOptionPane.INFORMATION_MESSAGE);
				if (dado1==dado2) {
					j.setEstaEnCarcel(false);
					accion=j.getNombre()+" sale de la c�rcel tirando dobles";
					j.setEstaEnCarcel(false);
					JOptionPane.showMessageDialog(this.getParent(), accion);
					PantallaPartida.resumenPartido("Turno "+PantallaPartida.nTurno+" - "+accion+"\n");
				} else {
					JOptionPane.showMessageDialog(this.getParent(), "No le ha salido dobles. No puede salir de la c�rcel");
				}
			} else if (opt==1) {
				if (j.getDineroBalance()<100) {
					JOptionPane.showMessageDialog(this.getParent(), "No tienes suficiente dinero para pagar la salida. Por favor, elija otra opci�n");
					casoCarcel(dado1, dado2, j);
				} else {
					j.setEstaEnCarcel(false);
					accion=j.getNombre()+" sale de la c�rcel pagando 100�";
					j.setDineroBalance(j.getDineroBalance()-100);
					JOptionPane.showMessageDialog(this.getParent(), accion);
					PantallaPartida.resumenPartido("Turno "+PantallaPartida.nTurno+" - "+accion+"\n");
					JOptionPane.showMessageDialog(this.getParent(), "Dado 1: "+dado1+"\nDado 2: "+dado2, "Tira "+j.getNombre(), JOptionPane.INFORMATION_MESSAGE);
				}
			} else if (opt==2) {
				if (j.getnCartasSalirCarcel()==0) {
					JOptionPane.showMessageDialog(this.getParent(), "No dispone de la carta 'SALGA DE LA CARCEL GRATIS'. Por favor, elija otra opci�n");
					casoCarcel(dado1, dado2, j);
				} else {
					j.setEstaEnCarcel(false);
					accion=j.getNombre()+" sale de la c�rcel usando la carta 'SALGA DE LA CARCEL GRATIS'";
					j.setnCartasSalirCarcel(j.getnCartasSalirCarcel()-1);
					PantallaPartida.resumenPartido("Turno "+PantallaPartida.nTurno+" - "+accion+"\n");
					JOptionPane.showMessageDialog(this.getParent(), "Dado 1: "+dado1+"\nDado 2: "+dado2, "Tira "+j.getNombre(), JOptionPane.INFORMATION_MESSAGE);
				}
			}
		}
    }
}
