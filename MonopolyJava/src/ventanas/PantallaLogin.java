package ventanas;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import clases.Usuario;
import componentes.BotonPropio;
import constantes.Constantes;

import javax.swing.JPasswordField;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;

/**
 * Clase que simula la pantalla de login
 * @author Darashc
 *
 */
public class PantallaLogin extends JPanel {
	private Ventana ventana; // Ventana de la que hereda el panel
	private JTextField txtUsuario; // Campo usuario
	private JPasswordField txtContrase�a; // Campo contrase�a
	private Connection bd; // Conexion a la base de datos
	private Statement loginStatement; // Statement de la base de datos
	
	/**
	 * Constructor de la pantalla login
	 * @param v Ventana
	 */
	public PantallaLogin(Ventana v) {
		super();
		try {
			bd=DriverManager.getConnection(Constantes.bdNombre, Constantes.bdUsuario, Constantes.bdContra);
			loginStatement=bd.createStatement();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(ventana, "Error conectando a la base de datos");
			e.printStackTrace();
		}
		this.ventana=v;
		initComponents();
	}
	
	/**
	 * Funci�n que pinta la pantalla login
	 */
	public void initComponents() {
		setBackground(new Color(163, 224, 174));
		setLayout(null);
		setSize(330, 320);
		
		JLabel lblUsuario = new JLabel("Nombre:");
		lblUsuario.setFont(new Font("Arial Nova", Font.PLAIN, 15));
		lblUsuario.setBounds(35, 25, 58, 14);
		add(lblUsuario);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a:");
		lblContrasea.setFont(new Font("Arial Nova Cond", Font.PLAIN, 15));
		lblContrasea.setBounds(35, 91, 80, 14);
		add(lblContrasea);
		
		txtUsuario = new JTextField();
		txtUsuario.setBounds(35, 50, 135, 20);
		add(txtUsuario);
		txtUsuario.setColumns(10);
		
		txtContrase�a = new JPasswordField();
		txtContrase�a.setBounds(35, 116, 135, 20);
		add(txtContrase�a);
		
		BotonPropio btnLogin = new BotonPropio("Login", 15);
		btnLogin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (loginUsuario()!=null) {
					ventana.setUsuarioLogeado(loginUsuario());
					ventana.cargarPantallaUsuario(); // Cargara la pantalla de inicio del usuario
				} else {
					JOptionPane.showMessageDialog(ventana, "Nombre/Contrase�a introducidos incorrectamente", "Login fallido", JOptionPane.ERROR_MESSAGE); // Si el login es incorrecto, muestra un mensaje de error
				}
			}
		});
		btnLogin.setBounds(35, 156, 89, 23);
		add(btnLogin);
		
		BotonPropio btnRegistrar = new BotonPropio("Registrar", 15); // Bot�n que va a la pantalla de registro del usuario
		btnRegistrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {  // Bot�n que vuelve a la pantalla inicio del juego
				ventana.cargarPantallaRegistro();
			}
		});
		btnRegistrar.setBounds(35, 223, 89, 23);
		add(btnRegistrar);
		
		BotonPropio btnVolver = new BotonPropio("Volver", 15);
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ventana.cargarPantallaInicio();
			}
		});
		btnVolver.setBounds(187, 223, 89, 23);
		add(btnVolver);
	}
	
	/**
	 * Funci�n que devuelve el usuario cogido de la base de datos
	 * @return El usuario logueado (Si lo existe en la base de datos, en cualquier otro caso, devolver� null)
	 */
	public Usuario loginUsuario() {
		try {
			ResultSet loginData=loginStatement.executeQuery("SELECT *"
					+ " FROM usuario"
					+ " WHERE name='"+txtUsuario.getText()+"' AND password='"+String.valueOf(txtContrase�a.getPassword())+"'");
			
			if (loginData.next()) {
				Usuario usuarioLogueado=new Usuario(loginData.getString("name"), loginData.getString("email"), loginData.getString("password"), loginData.getInt("nMatches"), loginData.getInt("nWins"), loginData.getString("lastConnection").equals("null")?null:LocalDateTime.parse(loginData.getString("lastConnection")));
				return usuarioLogueado;
			}
		} catch (SQLException e) {
			System.err.println("Error de conexi�n");
			e.printStackTrace();
		}
		
		return null;
	}
}
