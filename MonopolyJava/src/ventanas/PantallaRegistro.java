package ventanas;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import clases.Usuario;
import componentes.BotonPropio;
import constantes.Constantes;
import excepciones.EmailIncorrectoException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Clase que simula la pantalla registro
 * @author Darashc
 *
 */
public class PantallaRegistro extends JPanel {
	private Ventana ventana; // Ventana de la que hereda el panel
	private JTextField txtUsuario; // Campo Usuario
	private JTextField txtEmail; // Campo Email
	private JPasswordField txtContra1; // Primer campo de contrase�a
	private JPasswordField txtContra2; // Segundo campo de contrase�a
	private String msgError; // Mensaje de error
	private Connection bd; // Conexi�n de la base de datos
	private Statement registerStatement; // Statement de la base de datos
	
	public PantallaRegistro(Ventana v) {
		super();
		try {
			bd=DriverManager.getConnection(Constantes.bdNombre, Constantes.bdUsuario, Constantes.bdContra);
			registerStatement=bd.createStatement();
		} catch (SQLException e) {
			System.err.println("Error connectando a la base de datos");
			e.printStackTrace();
		}
		this.ventana=v;
		initComponents();
	}
	
	/**
	 * Funci�n que pinta la pantalla
	 */
	public void initComponents() {
		setBackground(new Color(163, 224, 174));
		setLayout(null);
		setSize(330, 400);
		
		JLabel lblUsuario = new JLabel("Nombre:");
		lblUsuario.setFont(new Font("Arial Nova", Font.PLAIN, 15));
		lblUsuario.setBounds(35, 25, 58, 14);
		add(lblUsuario);
		
		txtUsuario = new JTextField();
		txtUsuario.setBounds(35, 50, 160, 20);
		add(txtUsuario);
		txtUsuario.setColumns(10);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setFont(new Font("Arial Nova", Font.PLAIN, 15));
		lblEmail.setBounds(35, 91, 41, 14);
		add(lblEmail);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(35, 116, 160, 20);
		add(txtEmail);
		txtEmail.setColumns(10);
		
		JLabel lblContra1 = new JLabel("Contrase�a:");
		lblContra1.setFont(new Font("Arial Nova", Font.PLAIN, 15));
		lblContra1.setBounds(35, 157, 80, 14);
		add(lblContra1);
		
		txtContra1 = new JPasswordField();
		txtContra1.setBounds(35, 182, 160, 20);
		add(txtContra1);
		txtContra1.setColumns(10);
		
		JLabel lblContra2 = new JLabel("Confirmar contrase�a:");
		lblContra2.setFont(new Font("Arial Nova", Font.PLAIN, 15));
		lblContra2.setBounds(35, 223, 146, 14);
		add(lblContra2);
		
		txtContra2 = new JPasswordField();
		txtContra2.setBounds(35, 248, 160, 20);
		add(txtContra2);
		txtContra2.setColumns(10);
		
		
		BotonPropio btnVolver = new BotonPropio("Volver", 15);
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ventana.cargarPantallaInicio();
			}
		});
		btnVolver.setBounds(187, 305, 89, 23);
		add(btnVolver);
		
		BotonPropio btnRegistrar = new BotonPropio("Registrar", 15);
		btnRegistrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				registroUsuario();
				if (ventana.getUsuarioLogueado()!=null) { // Solo se va a poder cargar la pantalla de usuario si el usuario no es nulo
					ventana.cargarPantallaUsuario();
				}
			}
		});
		btnRegistrar.setBounds(35, 305, 89, 23);
		add(btnRegistrar);
	}
	
	/**
	 * Funci�n que comprueba la inserci�n de registro del usuario
	 * @return Si no hay ning�n fallo, true. Sino, false
	 */
	public boolean comprobarRegistro() {
		if (txtUsuario.getText().isEmpty()||txtEmail.getText().isEmpty()||String.valueOf(txtContra1.getPassword()).isEmpty()||String.valueOf(txtContra2.getPassword()).isEmpty()) { // Si todos los campos est�n vacios
			msgError="No ha rellenado los datos que le piden. Por favor, h�galo";
			return false;
		} else if (!String.valueOf(txtContra1.getPassword()).equals(String.valueOf(txtContra2.getPassword()))) { // Si coinciden las contrase�as introducidas en ambos campos
			msgError="Las contrase�as no coinciden";
			return false;
		}
		
		try {
			ResultSet rsNombre=registerStatement.executeQuery("SELECT name"
					+ " FROM usuario"
					+ " WHERE name='"+txtUsuario.getText()+"'");
			if (rsNombre.next()) { // Si el nombre introducido ya existe en la base de datos
				msgError="El nombre de usuario introducido ya existe";
				rsNombre.close();
				return false;
			}
			ResultSet rsEmail=registerStatement.executeQuery("SELECT email"
					+ " FROM usuario"
					+ " WHERE email='"+txtEmail.getText()+"'");
			if (rsEmail.next()) { // Si el email introducido ya existe en la base de datos
				msgError="El email introducido ya existe";
				rsEmail.close();
				return false;
			}
		} catch (SQLException e) {
			// Muestra si hay alg�n fallo a la hora de hacer alg�n ResultSet
			JOptionPane.showMessageDialog(ventana, "Ha habido un fallo a la comprobar su registro. Por favor, int�ntelo otra vez de nuevo", "Error ResultSet", JOptionPane.ERROR_MESSAGE);
		}
		
		return true;
	}
	
	/**
	 * Funcion que mete el usuario a la base de datos
	 * Solo se mete a la base de datos si la comprobacion de registro (comprobarRegistro()) devuelve true
	 * Salvo en el caso de que el email no se haya introducido correctamente. Para ello, se lanza la excepcion
	 */
	public void registroUsuario() {
		if (comprobarRegistro()==false) {
			JOptionPane.showMessageDialog(ventana, msgError, "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			try {
				Usuario usuarioRegistrado=new Usuario(txtUsuario.getText(), txtEmail.getText(), String.valueOf(txtContra1.getPassword()));
				registerStatement.execute("INSERT INTO usuario (name, email, password, nMatches, nWins, lastConnection) VALUES ('"+usuarioRegistrado.getNombre()+"', '"+usuarioRegistrado.getEmail()+"', '"+usuarioRegistrado.getContrase�a()+"', 0, 0, 'null')");
				JOptionPane.showMessageDialog(ventana, "�Registro con �xito!");
				registerStatement.close();
				ventana.setUsuarioLogeado(usuarioRegistrado);
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(ventana, "Error insertado usuario a la base de datos (Quizas porque el creador no sabe programar)", "Error", JOptionPane.ERROR_MESSAGE);
			} catch (EmailIncorrectoException e) {
				JOptionPane.showMessageDialog(ventana, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}
