package ventanas;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import clases.Usuario;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Clase que simula una ventana
 * @author Darashc
 *
 */
public class Ventana extends JFrame {
	private PantallaInicio pInicio; // La pantalla de inicio
	private PantallaLogin pLogin; // La pantalla de login
	private PantallaRegistro pRegistro; // La pantalla de registro
	private PantallaUsuario pUsuario; // La pantalla de usuario
	private PantallaDatosUsuario pDatosUsuario; // La pantalla de datos del usuario
	protected PantallaNuevaPartida pNuevaPartida; // La pantalla de nueva partida
	private PantallaPartida pPartida; // La pantalla de partida
	private Usuario usuarioLogueado; // Usuario logueado
	private Ventana thisRef; // Variable referenciada a ventana
	
	/**
	 * Constructor de ventana
	 */
	public Ventana() {
		this.thisRef=this;
		initComponents();
	}
	
	/**
	 * Funci�n que devuelve el usuario logueado
	 * @return El usuario logueado
	 */
	public Usuario getUsuarioLogueado() {
		return usuarioLogueado;
	}

	/**
	 * Funci�n que inicializa un usuario
	 * @param usuarioLogueado El usuario que se va a inicializar
	 */
	public void setUsuarioLogeado(Usuario usuarioLogueado) {
		this.usuarioLogueado = usuarioLogueado;
	}


	/**
	 * Funci�n que pinta la ventana
	 */
	public void initComponents() {
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				int opt=JOptionPane.showConfirmDialog(thisRef, "�Estas seguro de que quieres salir del juego?", "Salida", JOptionPane.YES_NO_OPTION); // Muestra un di�logo al intenta de cerrar la ventana
				if (opt==0) {
					if (usuarioLogueado!=null) {
						pUsuario.salirJuego();
					}
					if (thisRef.pPartida!=null) {
						PantallaPartida.resumenPartido("\nHas cerrado el juego");
					}
					thisRef.dispose();
				}
			}
		});
		this.setSize(600, 300);
		this.setTitle("Millionaire");
		this.pInicio=new PantallaInicio(this);
		this.setContentPane(pInicio);
		this.setVisible(true);
	}
	
	/**
	 * Funci�n que carga la pantalla login
	 */
	public void cargarPantallaLogin() {
		if (pLogin==null) {
			this.pLogin=new PantallaLogin(this);
		}
		this.setTitle("Login");
		this.pInicio.setVisible(false);
		this.setSize(330, 320);
		this.setContentPane(pLogin);
		this.pLogin.setVisible(true);
	}
	
	/**
	 * Funci�n que carga la pantalla registro
	 */
	public void cargarPantallaRegistro() {
		if (pRegistro==null) {
			this.pRegistro=new PantallaRegistro(this);
		}
		this.setTitle("Registro");
		this.pInicio.setVisible(false);
		this.setSize(330, 400);
		this.setContentPane(pRegistro);
		this.pRegistro.setVisible(true);
	}
	
	/**
	 * Funci�n que carga la pantalla de inicio
	 */
	public void cargarPantallaInicio() {
		this.getTitle();
		if (this.pLogin!=null) {
			this.pLogin.setVisible(false);
		} else if (this.pRegistro!=null) {
			this.pRegistro.setVisible(false);
		}
		this.setTitle("Millionaire");
		this.setSize(605, 300);
		this.setContentPane(pInicio);
		this.pInicio.setVisible(true);
	}
	
	/**
	 * Funci�n que carga la pantalla de usuario (Pantalla incial justo despu�s de registrarse/loguearse
	 * Esta pantalla solo empieza a aparecer si el usuario se registra/loguea correctamente
	 */
	public void cargarPantallaUsuario() {
		if (this.pDatosUsuario!=null) {
			this.pDatosUsuario.setVisible(false);
		}
		if (this.pNuevaPartida!=null) {
			this.pNuevaPartida.setVisible(false);
		}
		if (this.pUsuario==null) {
			this.pUsuario=new PantallaUsuario(this);
		}
		this.setTitle("�Bienvenido!");
		this.pLogin.setVisible(false);
		this.setSize(605, 300);
		this.setContentPane(pUsuario);
		this.pUsuario.setVisible(true);
	}
	
	/**
	 * Funci�n que cargar la pantalla de los datos de usuario
	 */
	public void cargarPantallaDatosUsuario() {
		if (this.pRegistro!=null) {
			this.pRegistro.setVisible(false);
		} else if (this.pLogin!=null) {
			this.pLogin.setVisible(false);
		}
		if (this.pDatosUsuario==null) {
			this.pDatosUsuario=new PantallaDatosUsuario(this);
		}
		this.setTitle("Datos Usuario");
		this.pUsuario.setVisible(false);
		this.setSize(330, 320);
		this.setContentPane(pDatosUsuario);
		this.pDatosUsuario.setVisible(true);
	}
	
	/**
	 * Funci�n que carga la pantalla de nueva partida
	 */
	public void cargarPantallaNuevaPartida() {
		if (this.pNuevaPartida==null) {
			this.pNuevaPartida=new PantallaNuevaPartida(this);
		}
		this.setTitle("Nueva Partida");
		this.pUsuario.setVisible(false);
		this.setSize(900, 900);
		this.setContentPane(pNuevaPartida);
		this.pNuevaPartida.setVisible(true);
	}
	
	/**
	 * Funci�n que carga la pantalla de la partida
	 */
	public void cargarNuevaPartida() {
		if (this.pPartida==null) {
			this.pPartida=new PantallaPartida(this);
		}
		this.setTitle("Partida");
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.setContentPane(pPartida);
		this.pPartida.setVisible(true);
	}
}
