package ventanas;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Clase que muestra la pantalla de datos del usuario logueado
 * @author Darashc
 *
 */
public class PantallaDatosUsuario extends JPanel {
	private Ventana ventana; // Ventana de la que va a heredar el panel
	
	public PantallaDatosUsuario(Ventana v) {
		super();
		this.ventana=v;
		initComponents();
	}
	
	/**
	 * Funci�n que pinta la pantalla
	 */
	public void initComponents() {
		setBackground(new Color(163, 224, 174));
		setLayout(null);
		setSize(330, 320);
		
		JLabel labelDatosUsuario = new JLabel(ventana.getUsuarioLogueado().datosUsuario());
		labelDatosUsuario.setFont(new Font("Verdana Pro Light", Font.PLAIN, 20));
		labelDatosUsuario.setBounds(10, 11, 310, 231);
		add(labelDatosUsuario);
		
		JButton btnVolver = new JButton("Volver"); // Bot�n que vuelve a la pantalla inicial del usuario
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ventana.cargarPantallaUsuario();
			}
		});
		btnVolver.setBounds(214, 245, 89, 23);
		add(btnVolver);
	}
}
