package ventanas;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import componentes.BotonPropio;

/**
 * Clase de pantalla inicio
 * Es la pantalla que se muestra al abrir la aplicaci�n
 * @author Darashc
 *
 */
public class PantallaInicio extends JPanel {
	private Ventana ventana; // Ventana de la que hereda la pantalla
	private BufferedImage logo; // Logo del juego
	
	/**
	 * Constructor de la pantalla inicio
	 * @param v Ventana
	 */
	public PantallaInicio(Ventana v) {
		super();
		this.ventana=v;
		initComponents();
	}
	
	/**
	 * Funci�n que pinta la pantalla
	 */
	public void initComponents() {
		setBackground(new Color(163, 224, 174));
		setLayout(null);
		setSize(605, 300);
		
		JPanel panel = new JPanel();
		panel.setBounds(140, 34, 304, 95);
		try {
			logo=ImageIO.read(new File("./Imagenes/MillonarioLogo.png"));
		} catch (IOException e) {
			JOptionPane.showMessageDialog(ventana, "Error cargando logo del juego");
			e.printStackTrace();
		}
		JLabel labelImagen=new JLabel(new ImageIcon(logo)); // Label para mostrar el logo del juego
		panel.add(labelImagen);
		panel.setBackground(new Color(163, 224, 174));
		add(panel);
		
		BotonPropio btnJugarComoInvitado = new BotonPropio("Jugar como invitado", 12);
		btnJugarComoInvitado.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ventana.cargarNuevaPartida(); // Si juega como invitado, va directamente apartido. Sin opci�n de elegir el N� jugadores y las figuras
			}
		});
		btnJugarComoInvitado.setBounds(65, 205, 147, 23);
		add(btnJugarComoInvitado);
		
		BotonPropio btnJugarComoUsuario = new BotonPropio("Jugar como usuario", 12);
		btnJugarComoUsuario.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ventana.cargarPantallaLogin(); // El usuario deber� loguearse/registrarse para poder jugar como usuario. Con opci�n de elegir el N� jugadores, las figuras
			}
		});
		btnJugarComoUsuario.setFont(new Font("Corbel", Font.PLAIN, 12));
		btnJugarComoUsuario.setBounds(360, 205, 147, 23);
		add(btnJugarComoUsuario);
		
		this.setVisible(true);
	}
}
