package ventanas;

import java.awt.Color;

import javax.swing.JPanel;

import clases.Figura;
import componentes.BotonPropio;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.BorderLayout;
import javax.swing.border.LineBorder;
import javax.swing.ButtonGroup;

public class PantallaNuevaPartida extends JPanel {
	private Ventana ventana; // Ventana
	private PantallaNuevaPartida thisRef; // Variable que referencia a la propia clase
	private int aux; // Auxiliar
	private Figura[] figuras; // Figuras del juego
	private JPanel panelFigura1; // Panel de figura para jugador 1
	private JPanel panelFigura2; // Panel de figura para jugador 2
	private JPanel panelFigura3; // Panel de figura para jugador 3
	private JPanel panelFigura4; // Panel de figura para jugador 4
	private JPanel panel; // Panel de datos para jugador 1
	private JPanel panel2; // Panel de datos para jugador 2
	private JPanel panel3; // Panel de datos para jugador 3
	private JPanel panel4; // Panel de datos para jugador 4
	private JLabel lblImagen; // Label de imagen para jugador 1
	private JLabel lblImagen2; // Label de imagen para jugador 2
	private JLabel lblImagen3; // Label de imagen para jugador 3
	private JLabel lblImagen4; // Label de imagen para jugador 4
	protected JComboBox[] comboBoxFigura; // ComboBox para la seleccion de figura
	private DefaultComboBoxModel seleccionFigura1; // Modelo para ComboBox
	private DefaultComboBoxModel seleccionFigura2; // Modelo para ComboBox
	private DefaultComboBoxModel seleccionFigura3; // Modelo para ComboBox
	private DefaultComboBoxModel seleccionFigura4; // Modelo para ComboBox
	protected int nJugadores; // N� Jugadores
	
	/**
	 * Constructor de PantallaNuevaPartida
	 * @param v Ventana
	 */
	public PantallaNuevaPartida(Ventana v) {
		thisRef=this;
		this.comboBoxFigura=new JComboBox[4];
		this.aux=1;
		this.ventana=v;
		this.figuras=Figura.inicializarFiguras();
		initComponents();
	}
	 /**
	  * Funcion que pinta la pantalla
	  */
	public void initComponents() {
		setBackground(new Color(163, 224, 174));
		setSize(900, 900);
		setLayout(null);
		
		BotonPropio btnVolver = new BotonPropio("Volver", 13); // Bot�n que vuelve a la pantalla inicial del usuario
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ventana.cargarPantallaUsuario();
			}
		});
		btnVolver.setBounds(655, 665, 113, 23);
		add(btnVolver);
		
		BotonPropio btnIniciarPartida = new BotonPropio("Iniciar Partida", 13); // Bot�n que inicia partida
		btnIniciarPartida.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ventana.cargarNuevaPartida();
			}
		});
		btnIniciarPartida.setBounds(90, 665, 113, 23);
		add(btnIniciarPartida);
		
		panelFigura1 = new JPanel();
		panel = new JPanel();
		lblImagen = new JLabel();
		comboBoxFigura[0] = new JComboBox();
		seleccionFigura1=new DefaultComboBoxModel(new String[] {"--Selecciona una figura--", figuras[0].getNombre(), figuras[1].getNombre(), figuras[2].getNombre(), figuras[3].getNombre(), figuras[4].getNombre(), figuras[5].getNombre(), figuras[6].getNombre(), figuras[7].getNombre()});
		comboBoxFigura[0] .setModel(seleccionFigura1);
		
		panelFigura2 = new JPanel();
		panel2 = new JPanel();
		lblImagen2 = new JLabel();
		comboBoxFigura[1] = new JComboBox();
		seleccionFigura2=new DefaultComboBoxModel(new String[] {"--Selecciona una figura--", figuras[0].getNombre(), figuras[1].getNombre(), figuras[2].getNombre(), figuras[3].getNombre(), figuras[4].getNombre(), figuras[5].getNombre(), figuras[6].getNombre(), figuras[7].getNombre()});
		comboBoxFigura[1].setModel(seleccionFigura2);
		
		panelFigura3 = new JPanel();
		panel3 = new JPanel();
		lblImagen3 = new JLabel();
		comboBoxFigura[2] = new JComboBox();
		seleccionFigura3=new DefaultComboBoxModel(new String[] {"--Selecciona una figura--", figuras[0].getNombre(), figuras[1].getNombre(), figuras[2].getNombre(), figuras[3].getNombre(), figuras[4].getNombre(), figuras[5].getNombre(), figuras[6].getNombre(), figuras[7].getNombre()});
		comboBoxFigura[2].setModel(seleccionFigura3);
		
		panelFigura4 = new JPanel();
		panel4 = new JPanel();
		lblImagen4 = new JLabel();
		comboBoxFigura[3] = new JComboBox();
		seleccionFigura4=new DefaultComboBoxModel(new String[] {"--Selecciona una figura--", figuras[0].getNombre(), figuras[1].getNombre(), figuras[2].getNombre(), figuras[3].getNombre(), figuras[4].getNombre(), figuras[5].getNombre(), figuras[6].getNombre(), figuras[7].getNombre()});
		comboBoxFigura[3].setModel(seleccionFigura4);
		
		BotonPropio btn2Jugadores = new BotonPropio("2", 13); // Bot�n que muestra panel para 2 jugadores
		btn2Jugadores.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				nJugadores=2;
				panelJugador1();
				panelJugador2();
				panelFigura3.setVisible(false);
				panelFigura4.setVisible(false);
				comboBoxFigura[0].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						seleccionarFigura(comboBoxFigura[0], lblImagen);
					}
				});
				
				comboBoxFigura[1].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						seleccionarFigura(comboBoxFigura[1], lblImagen2);
					}
				});
			}
		});
		btn2Jugadores.setBounds(449, 33, 50, 23);
		add(btn2Jugadores);
		
		BotonPropio btn3Jugadores = new BotonPropio("3", 13); // Bot�n que muestra panel para 3 jugadores
		btn3Jugadores.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				nJugadores=3;
				panelJugador1();
				panelJugador2();
				panelJugador3();
				panelFigura4.setVisible(false);
				comboBoxFigura[0].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						seleccionarFigura(comboBoxFigura[0], lblImagen);
					}
				});
				
				comboBoxFigura[1].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						seleccionarFigura(comboBoxFigura[1], lblImagen2);
					}
				});
				
				comboBoxFigura[2].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						seleccionarFigura(comboBoxFigura[2], lblImagen3);
					}
				});
		}});
		btn3Jugadores.setBounds(605, 33, 50, 23);
		add(btn3Jugadores);
		
		BotonPropio btn4Jugadores = new BotonPropio("4", 13); // Bot�n que muestra panel para 4 jugadores
		btn4Jugadores.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				nJugadores=4;
				panelJugador1();
				panelJugador2();
				panelJugador3();
				panelJugador4();
				
				comboBoxFigura[0].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						seleccionarFigura(comboBoxFigura[0], lblImagen);
					}
				});
				
				comboBoxFigura[1].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						seleccionarFigura(comboBoxFigura[1], lblImagen2);
					}
				});
				
				comboBoxFigura[2].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						seleccionarFigura(comboBoxFigura[2], lblImagen3);
					}
				});
				
				comboBoxFigura[3].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						seleccionarFigura(comboBoxFigura[3], lblImagen4);
					}
				});
		}});
		btn4Jugadores.setBounds(761, 33, 50, 23);
		add(btn4Jugadores);
		
		JLabel lblNJugadores = new JLabel("N\u00BA Jugadores:");
		lblNJugadores.setBounds(90, 37, 113, 14);
		add(lblNJugadores);
	}
	
	/**
	 * Funcion que pinta el panel para Jugador 1
	 */
	public void panelJugador1() {
		panelFigura1.setLayout(null);
		panelFigura1.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		panelFigura1.setBounds(121, 75, 280, 280);
		panelFigura1.setOpaque(true);
		add(panelFigura1);
		
		panel.setBounds(50, 43, 200, 200);
		panelFigura1.add(panel);
		
		panel.add(lblImagen);
		
		comboBoxFigura[0].setBounds(50, 11, 200, 20);
		panelFigura1.add(comboBoxFigura[0]);
		
		panelFigura1.setVisible(true);
		panelFigura1.repaint();
	}
	
	/**
	 * Funcion que pinta el panel para Jugador 2
	 */
	public void panelJugador2() {
		panelFigura2.setLayout(null);
		panelFigura2.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		panelFigura2.setBounds(450, 75, 280, 280);
		panelFigura2.setOpaque(true);
		add(panelFigura2);
		
		panel2.setBounds(50, 42, 200, 200);
		panelFigura2.add(panel2);
		
		panel2.add(lblImagen2);
		
		lblImagen.setSize(200, 200);
		panel.add(lblImagen);
		
		comboBoxFigura[1].setBounds(50, 11, 200, 20);
		panelFigura2.add(comboBoxFigura[1]);
		
		panelFigura2.setVisible(true);
		panelFigura2.repaint();
	}
	
	/**
	 * Funcion que pinta el panel para Jugador 3
	 */
	public void panelJugador3() {
		panelFigura3.setLayout(null);
		panelFigura3.setOpaque(true);
		panelFigura3.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		panelFigura3.setBounds(121, 370, 280, 280);
		add(panelFigura3);
		
		panel3.setBounds(50, 43, 200, 200);
		panelFigura3.add(panel3);
		
		panel3.add(lblImagen3);
		
		comboBoxFigura[2].setBounds(50, 11, 200, 20);
		panelFigura3.add(comboBoxFigura[2]);
		
		panelFigura3.setVisible(true);
		panelFigura3.repaint();
	}
	
	/**
	 * Funcion que pinta el panel para Jugador 4
	 */
	public void panelJugador4() {
		panelFigura4.setLayout(null);
		panelFigura4.setOpaque(true);
		panelFigura4.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		panelFigura4.setBounds(450, 370, 280, 280);
		add(panelFigura4);
		
		panel4.setBounds(50, 43, 200, 200);
		panelFigura4.add(panel4);
		
		panel4.add(lblImagen4);
		
		comboBoxFigura[3].setBounds(50, 11, 200, 20);
		panelFigura4.add(comboBoxFigura[3]);
		
		panelFigura4.setVisible(true);
		panelFigura4.repaint();
	}
	
	/**
	 * Funci�n para seleccionar figura del comboBox
	 * @param c comboBox de figura
	 * @param l label de Imagen de figura
	 */
	public void seleccionarFigura(JComboBox c, JLabel l) {
		int fSeleccionada=c.getSelectedIndex();
		if (fSeleccionada!=0) {
			l.setIcon(new ImageIcon(figuras[fSeleccionada-1].getFigura()));
			aux=fSeleccionada-1;
		} else {
			l.setIcon(new ImageIcon(figuras[aux].getFigura()));
		}
	}
}
