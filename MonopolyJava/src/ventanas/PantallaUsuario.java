package ventanas;

import java.awt.Color;

import javax.swing.JPanel;

import componentes.BotonPropio;
import constantes.Constantes;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFrame;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import javax.swing.SwingConstants;

/**
 * Funci�n que simula la pantalla del usuario
 * @author Darashc
 *
 */
public class PantallaUsuario extends JPanel {
	private Ventana ventana; // Ventana de la que va a heredar el panel
	
	/**
	 * Constructor de la pantalla usuario
	 * @param v Ventana
	 */
	public PantallaUsuario (Ventana v) {
		super();
		this.ventana=v;
		initComponents();
	}
	
	/**
	 * Funci�n que pinta la pantalla del usuario
	 */
	public void initComponents() {
		setBackground(new Color(163, 224, 174));
		setSize(605, 300);
		setLayout(null);
		
		JLabel lblbienvenido = new JLabel("\u00A1Bienvenido, "+ventana.getUsuarioLogueado().getNombre()+"!");
		lblbienvenido.setHorizontalAlignment(SwingConstants.CENTER);
		lblbienvenido.setBounds(43, 87, 486, 32);
		lblbienvenido.setFont(new Font("Verdana Pro Light", Font.PLAIN, 25));
		add(lblbienvenido);
		
		BotonPropio btnNuevaPartida = new BotonPropio("Nueva partida", 13); // Bot�n que carga la pantalla de la configuraci�n para la nueva partida
		btnNuevaPartida.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ventana.cargarPantallaNuevaPartida();
			}
		});
		btnNuevaPartida.setBounds(10, 225, 165, 23);
		add(btnNuevaPartida);
		
		BotonPropio btnHistorial = new BotonPropio("Tus datos", 13); // Bot�n que muestra todos los datos del usuario 
		btnHistorial.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ventana.cargarPantallaDatosUsuario();
			}
		});
		btnHistorial.setBounds(212, 225, 165, 23);
		add(btnHistorial);
		
		BotonPropio btnSalir = new BotonPropio("Salir", 13); // Bot�n que cierra el juego
		btnSalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int opt=JOptionPane.showConfirmDialog(ventana, "�Estas seguro de que quieres salir del juego?", "Salida", JOptionPane.YES_NO_OPTION);
				if (opt==0) { // En el dialogo, el usuario pulsa "Si"
					salirJuego();
					ventana.dispose();
				}
			}
		});
		btnSalir.setBounds(415, 225, 165, 23);
		add(btnSalir);
	}
	
	/**
	 * Funci�n que simula una salida del juego
	 * Guarda en la base de datos de la fecha y el tiempo en la que el usuario tuvo su �ltima conexi�n
	 */
	public void salirJuego() {
		Connection bd;
		Statement logoutStatement;
		
		try {
			bd=DriverManager.getConnection(Constantes.bdNombre, Constantes.bdUsuario, Constantes.bdContra);
			logoutStatement=bd.createStatement();
			
			logoutStatement.execute("UPDATE usuario"
					+ " SET lastConnection = '"+LocalDateTime.now().toString()+"'"
					+ " WHERE name='"+ventana.getUsuarioLogueado().getNombre()+"'");
			ventana.getUsuarioLogueado().setLastConnection(LocalDateTime.now().toString());
			
			logoutStatement.close();
			bd.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
