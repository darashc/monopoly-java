package ventanas;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import clases.Casilla;
import clases.Figura;
import clases.Jugador;
import constantes.Constantes;

import java.awt.Cursor;

import javax.swing.border.LineBorder;

import javax.swing.JButton;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PantallaPartida extends JPanel {
	private Ventana ventana; // Ventana
	private Figura[] figuras; // Figuras
	public static Jugador[] jugadores; // Jugadores
	private JPanel[] panelJugador; // Panel con datos de jugadores
	private static JLabel[] lblFigura; // Label de imagenes de figuras
	public static JLabel[] lblDinero; // Label de dinero de jugadores
	private static Casilla[] casillas; // Casilla que muestra eltablero
	private JPanel pnlCasillaDetallada; // Panel de la casilla detallada
	private JLabel lblCasillaDetallada; // Label de la casilla detallada
	private int dado1; // Dado 1
	private int dado2; // Dado 2
	public static int nTurno; // N� de turno en la que se encuentra el partido
	private PantallaPartida thisRef; // Variable que hace referencia a la propia clase
	
	/**
	 * Constructor de PantallaPartida
	 * @param v Ventana
	 */
	public PantallaPartida (Ventana v) {
		this.ventana=v;
		this.dado1=0;
		this.dado2=0;
		this.thisRef=this;
		this.nTurno=1;
		this.panelJugador=new JPanel[(ventana.pNuevaPartida)==null?2:ventana.pNuevaPartida.nJugadores];
		this.lblFigura=new JLabel[(ventana.pNuevaPartida)==null?2:ventana.pNuevaPartida.nJugadores];
		this.lblDinero=new JLabel[(ventana.pNuevaPartida)==null?2:ventana.pNuevaPartida.nJugadores];
		this.casillas=Casilla.inicializarCasillas();
		
		this.figuras=Figura.inicializarFiguras();
		if (ventana.getUsuarioLogueado()==null) {
			Random r=new Random();
			jugadores=new Jugador[2];
			this.jugadores[0]=new Jugador("Jugador 1", figuras[r.nextInt(8)]);
			this.jugadores[1]=new Jugador("IA", figuras[r.nextInt(8)]);
		} else {
			jugadores=new Jugador[ventana.pNuevaPartida.nJugadores];
			this.jugadores[0]=new Jugador(ventana.getUsuarioLogueado().getNombre(), figuras[ventana.pNuevaPartida.comboBoxFigura[0].getSelectedIndex()-1]);
			for (int i=1; i<ventana.pNuevaPartida.nJugadores; i++) {
				this.jugadores[i]=new Jugador("IA "+i, figuras[ventana.pNuevaPartida.comboBoxFigura[i].getSelectedIndex()-1]);
			}
		}
		initComponents();
	}
	
	/**
	 * Funcion que pinta la pantalla
	 */
	public void initComponents() {
		setBackground(new Color(163, 224, 174));
		setLayout(null);
		setSize(1380, 680);
		
		JButton btnDados = new JButton("Tirar Dados");
		btnDados.setBounds(80, 152, 150, 35);
		
		JButton btnFinTurno = new JButton("Fin Turno");
		btnFinTurno.setBounds(80, 152, 150, 35);
		
		JButton btnHipotecar = new JButton("Hipotecar");
		btnHipotecar.setBounds(80, 232, 150, 35);
		
		JButton btnRedimir = new JButton("Redimir");
		btnRedimir.setBounds(80, 312, 150, 35);
		
		JButton btnBancarrota = new JButton("Bancarrota");
		btnBancarrota.setBounds(575, 195, 150, 35);
		
		JButton btnComenzar = new JButton("�Comenzar Partida!");
		btnComenzar.setBounds(575, 195, 150, 35);
		add(btnComenzar);
		
		String logFigura="";
		for (int i=0; i<((ventana.pNuevaPartida)==null?2:ventana.pNuevaPartida.nJugadores); i++) {
			logFigura+="\nFigura seleccionada para "+jugadores[i].getNombre()+": "+jugadores[i].getFiguraSeleccionada().getNombre();
		}
		
		casillasDetalladas();
		mostrarTablero();
		panelJugadores();
		resumenPartido("Resumen Partido\n"
				+ "-------------------------\n"
				+ "N� de jugadores: "+jugadores.length+"\n"
				+ "�Jugador 1 es un usuario?"+(ventana.getUsuarioLogueado()==null?" No, esta jugando como invitado":" Si, esta logueado como "+ventana.getUsuarioLogueado().getNombre())
				+ logFigura+"\n"
				+ "-------------------------\n");
		
		btnComenzar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {			
				ComenzarPartida();
				remove(btnComenzar);
				add(btnDados);
				add(btnHipotecar);
				add(btnRedimir);
			}
		});
		
		btnBancarrota.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {	
				String accion="";
				jugadores[0].setEstaBancarrota(true);
				accion=jugadores[0].getNombre()+" ha declarado bancarrota.";
				JOptionPane.showMessageDialog(ventana, accion, "Bancarrota", JOptionPane.INFORMATION_MESSAGE);
				resumenPartido(accion+"\n\nJugador ha salido del partido");
				thisRef.setVisible(false);
				if (ventana.getUsuarioLogueado()!=null) {
					actualizarDatos(false);
				}
				ventana.cargarPantallaInicio();
			}
		});
		
		btnDados.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (jugadores[0].isEstaEnCarcel()) {
					dado1=jugadores[0].tirarDados();
					dado2=jugadores[0].tirarDados();
					casillas[jugadores[0].getPosicion()].casoCarcel(dado1, dado2, jugadores[0]);
					if (!jugadores[0].isEstaEnCarcel()) {
						moverFigura(jugadores[0], lblFigura[0], 0);
					}
				} else {
					do {
						dado1=jugadores[0].tirarDados();
						dado2=jugadores[0].tirarDados();
						JOptionPane.showMessageDialog(ventana, "Dado 1: "+dado1+"\nDado 2: "+dado2, "Tira "+jugadores[0].getNombre(), JOptionPane.INFORMATION_MESSAGE);
						moverFigura(jugadores[0], lblFigura[0], 0);
						if (dado1==dado2&&!jugadores[0].isEstaEnCarcel()) {
							JOptionPane.showMessageDialog(ventana, "�Le ha tocado dobles! �Vuelva a tirar!");
						}
					} while (dado1==dado2&&!jugadores[0].isEstaEnCarcel());
				}
				if (jugadores[0].getDineroBalance()<0) {
					declararBancarrota();
				}
				remove(btnDados);
				add(btnFinTurno);
				ventana.repaint();
			}
		});
		
		btnFinTurno.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				remove(btnFinTurno);
				ventana.repaint();
				for (int i=1; i<jugadores.length; i++) {
					if (!jugadores[i].isEstaBancarrota()) {
						if (jugadores[i].isEstaEnCarcel()) {
							dado1=jugadores[i].tirarDados();
							dado2=jugadores[i].tirarDados();
							casillas[jugadores[i].getPosicion()].casoCarcel(dado1, dado2, jugadores[i]);
							if (!jugadores[i].isEstaEnCarcel()) {
								moverFigura(jugadores[i], lblFigura[i], i);
							}
						} else {
							do {
								dado1=jugadores[i].tirarDados();
								dado2=jugadores[i].tirarDados();
								JOptionPane.showMessageDialog(ventana, "Dado 1: "+dado1+"\nDado 2: "+dado2, "Tira "+jugadores[i].getNombre(), JOptionPane.INFORMATION_MESSAGE);
								moverFigura(jugadores[i], lblFigura[i], i);
								lblDinero[i].setText(jugadores[i].getDineroBalance()+" �");
								if (dado1==dado2) {
									JOptionPane.showMessageDialog(ventana, "�Le ha tocado dobles! �Vuelva a tirar!");
								}
								if (jugadores[i].getPropiedadesRedimibles().size()!=0&&jugadores[i].getDineroBalance()>=450) {
									jugadores[i].funcionRedimir();
								}
							} while (dado1==dado2);
						}
						if (jugadores[i].getDineroBalance()<0&&jugadores[i].getPropiedadesHipotecables().size()==0) {
							String accion="";
							jugadores[i].setEstaBancarrota(true);
							accion=jugadores[i].getNombre()+" ha declarado bancarrota.";
							JOptionPane.showMessageDialog(ventana, accion, "Bancarrota", JOptionPane.INFORMATION_MESSAGE);
							resumenPartido(accion);
						}
					}
				}
				nTurno++;
				add(btnDados);
				ventana.repaint();
			}
		});
		
		btnHipotecar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				jugadores[0].funcionHipotecar();
				lblDinero[0].setText(jugadores[0].getDineroBalance()+" �");
			}
		});
		
		btnRedimir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				jugadores[0].funcionRedimir();
				lblDinero[0].setText(jugadores[0].getDineroBalance()+" �");
			}
		});
	}
	
	/**
	 * Funcio�n que pinta los paneles de los jugadores con todos los detalles (Figura, dinero)
	 */
	public void panelJugadores() {
		int posY=40;
		for (int i=0; i<panelJugador.length; i++) {
			panelJugador[i]=new JPanel();
			panelJugador[i].setBorder(new LineBorder(new Color(0, 0, 0)));
			panelJugador[i].setLayout(null);
			panelJugador[i].setBounds(1085, posY, 200, 100);
			add(panelJugador[i]);
			
			JLabel lbl = new JLabel();
			JPanel figura = new JPanel();
			lblFigura[i] = new JLabel();
			lblDinero[i] = new JLabel();
			
			lbl.setBounds(72, 5, 60, 14);
			panelJugador[i].add(lbl);
			figura.setBounds(10, 29, 60, 60);
			figura.setOpaque(false);
			panelJugador[i].add(figura);
			figura.add(lblFigura[i]);
			lblDinero[i].setBounds(105, 48, 46, 14);
			panelJugador[i].add(lblDinero[i]);
			
			lbl.setText(jugadores[i].getNombre());
			lblFigura[i].setIcon(new ImageIcon(jugadores[i].getFiguraSeleccionada().getFigura().getScaledInstance(35, 40, BufferedImage.SCALE_SMOOTH)));
			lblDinero[i].setText(String.valueOf(jugadores[i].getDineroBalance())+" �");

			
			posY+=150;
		}
	}
	
	/**
	 * Funcion que muestra las casillas detalladas
	 */
	public void casillasDetalladas() {
		pnlCasillaDetallada = new JPanel();
		lblCasillaDetallada = new JLabel();
		lblCasillaDetallada.setSize(165, 250);
		pnlCasillaDetallada.setOpaque(false);
		pnlCasillaDetallada.add(lblCasillaDetallada);
		pnlCasillaDetallada.setVisible(false);
		pnlCasillaDetallada.setBounds(525, 229, 250, 255);
		add(pnlCasillaDetallada);
	}
	
	/**
	 * Funcion que muestra el tablero del juego
	 */
	public void mostrarTablero() {
		int x=900;
		int y=570;
		for (int i=0; i<10; i++) {
			casillaAPanel(i, x, y);
			x-=casillas[i+1].getImagen().getWidth();
		}
		for (int i=10; i<20; i++) {
			casillaAPanel(i, x, y);
			y-=casillas[i+1].getImagen().getHeight();
		}
		for (int i=20; i<30; i++) {
			casillaAPanel(i, x, y);
			x+=casillas[i].getImagen().getWidth();
		}
		for (int i=30; i<40; i++) {
			casillaAPanel(i, x, y);
			y+=casillas[i].getImagen().getHeight();
		}
	}
	
	/**
	 * Funcion para a�adir las casillas a un panel
	 * @param i Posicion
	 * @param x Eje x para SetBounds
	 * @param y Eje y para SetBounds
	 */
	public void casillaAPanel(int i, int x, int y) {
		casillas[i].setBounds(x, y, casillas[i].getImagen().getWidth(), casillas[i].getImagen().getHeight());
		casillas[i].setOpaque(false);
		add(casillas[i]);
		
		casillas[i].addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				try {
					lblCasillaDetallada.setIcon(new ImageIcon(ImageIO.read(new File("./imagenes/CasillasExtended/"+i+".png"))));
					casillas[i].setCursor(new Cursor(Cursor.HAND_CURSOR));
					pnlCasillaDetallada.setVisible(true);
				} catch (IOException e) {
					JOptionPane.showMessageDialog(ventana, "Error cargando im�genes", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
			@Override
			public void mouseExited(MouseEvent e) {
				pnlCasillaDetallada.setVisible(false);
			}
		});
	}
	
	/**
	 * Funcion que muestra la figura en el tablero
	 * @param i Posicion de la casilla
	 * @param lbl Label de la figura
	 * @param index Indice del jugador
	 */
	public static void mostrarFiguraEnTablero(int i, JLabel lbl, int index) {
		lblFigura[index]=new JLabel();
		lblFigura[index].setIcon(lbl.getIcon());
		lblFigura[index].setOpaque(false);
		casillas[i].add(lblFigura[index]);
		casillas[i].setComponentZOrder(lblFigura[index], 0);
	}
	
	/**
	 * Funci�n para comenzar partida
	 */
	public void ComenzarPartida() {
		dado1=0;
		dado2=0;
		
		for (int i=0; i<jugadores.length; i++) {
			casillas[jugadores[i].getPosicion()].remove(lblFigura[i]);
			mostrarFiguraEnTablero(jugadores[0].getPosicion(), lblFigura[i], i);
			figuraVisible(jugadores[i]);
		}
	}
	
	/**
	 * Funci�n para mover figura
	 * @param jug Jugador
	 * @param lbl Label de figura
	 * @param index indice del jugador
	 */
	public void moverFigura(Jugador jug, JLabel lbl, int index) {		
		casillas[jug.getPosicion()].remove(lblFigura[index]);
		if (jug.getPosicion()+(dado1+dado2)<=39) {
			jug.setPosicion(jug.getPosicion()+(dado1+dado2));
		} else {
			jug.setPosicion(jug.getPosicion()+(dado1+dado2)-40);
			jug.setDineroBalance(jug.getDineroBalance()+200);
			JOptionPane.showMessageDialog(ventana, "Has pasado por la casilla GO, obtienes 200�");
			lblDinero[index].setText(jug.getDineroBalance()+" �");
		}
		mostrarFiguraEnTablero(jug.getPosicion(), lbl, index);
		figuraVisible(jug);
		
		switch (casillas[jug.getPosicion()].getTipo()) {
			case NO_PARKING:
				String accion=jug.getNombre()+" ha aparcado en una zona donde est� prohibido, su castigo ser� ir a la c�rcel";
				JOptionPane.showMessageDialog(ventana, accion);
				casillas[jug.getPosicion()].remove(lblFigura[index]);
				jug.setPosicion(10);
				jug.setEstaEnCarcel(true);
				mostrarFiguraEnTablero(jug.getPosicion(), lbl, index);
				figuraVisible(jug);
				resumenPartido("Turno "+PantallaPartida.nTurno+" - "+accion+"\n");
				break;
			case CIUDAD:
				casillas[jug.getPosicion()].casoCiudad(jug);
				for (int i=0; i<jugadores.length; i++) {
					lblDinero[i].setText(jugadores[i].getDineroBalance()+" �");
				}
				break;
			case ESTACION:
				casillas[jug.getPosicion()].casoEstacion(jug);
				for (int i=0; i<jugadores.length; i++) {
					lblDinero[i].setText(jugadores[i].getDineroBalance()+" �");
				}
				break;
			case SUERTE:
				casillas[jug.getPosicion()].casoSuerte(jug, lblFigura[index], index);
				lblDinero[index].setText(jug.getDineroBalance()+" �");
				switch (casillas[jug.getPosicion()].getTipo()) {
					case CIUDAD:
						casillas[jug.getPosicion()].casoCiudad(jug);
						lblDinero[index].setText(jug.getDineroBalance()+" �");
						break;
					case ESTACION:
						casillas[jug.getPosicion()].casoEstacion(jug);
						lblDinero[index].setText(jug.getDineroBalance()+" �");
						break;
					case ARCA_COMUNAL:
						casillas[jug.getPosicion()].casoArcaComunal(jug, lblFigura[index], index);
						lblDinero[index].setText(jug.getDineroBalance()+" �");
						break;
					default:
						break;
				}
				break;
			case ARCA_COMUNAL:
				casillas[jug.getPosicion()].casoArcaComunal(jug, lblFigura[index], index);
				lblDinero[index].setText(jug.getDineroBalance()+" �");
				break;
			case TASAS:
				casillas[jug.getPosicion()].casoTasas(jug.getPosicion(), jug);
				lblDinero[index].setText(jug.getDineroBalance()+" �");
				break;
			case VISITA_CARCEL:
				resumenPartido("Turno "+nTurno+" - "+jug.getNombre()+" ha visitado carcel");
				break;
			case SPA:
				resumenPartido("Turno "+nTurno+" - "+jug.getNombre()+" ha conseguido un masaje gratis en un spa");
			default:
				break;
		}
	}
	
	/**
	 * Funcion que hace visible a jugador
	 * @param jug Jugador
	 */
	public void figuraVisible(Jugador jug) {
		casillas[jug.getPosicion()].setVisible(false);
		ventana.repaint();
		casillas[jug.getPosicion()].setVisible(true);
	}
	
	/**
	 * Funci�n que imprime las acciones realizadas en un archivo de texto
	 * @param accion
	 */
	public static void resumenPartido(String accion) {
        FileWriter log = null;
        try {
        	File logFile=new File("./resumenPartido.log");
            logFile.createNewFile();
            String stringLog=comprobarLog(logFile);
            log=new FileWriter(logFile);
            log.append(stringLog);
            log.write(accion);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,"Error en el log del Partido");
			e.printStackTrace();
		} finally {
			try {
				log.flush();
				log.close();
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Error guardando acciones en el log");
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Funcion que compruba si hay texto en el archivo pasado por par�metro
	 * @param log Archivo de texto
	 * @return texto
	 * @throws FileNotFoundException Si el archivo no es encontrado
	 * @throws IOException Si hay fallo a la hora de leer el archivo
	 */
	public static String comprobarLog(File log) throws FileNotFoundException, IOException {
        String ret="";
        String aux="";
        BufferedReader leerLog=new BufferedReader(new FileReader(log));
        
        do {
            aux=leerLog.readLine();
            if (aux!=null) {
                ret+=aux+"\n";
            }
            
        } while (aux!=null);
        
        leerLog.close();
        return ret;
    }
	
	/**
	 * Funcion que actualiza los datos del juagdor (Si el usuario est� logueado)
	 * @param victoria True si ha conseguido vicotria en la partida, false si no.
	 */
	public void actualizarDatos(boolean victoria) {
		Connection bd;
		Statement actualizarStatement;
		int numeroPartidas=0;
		int numeroVictorias=0;
		
		try {
			bd=DriverManager.getConnection(Constantes.bdNombre, Constantes.bdUsuario, Constantes.bdContra);
			actualizarStatement=bd.createStatement();
			
			ResultSet resUsuarioData=actualizarStatement.executeQuery("SELECT nMatches, nWins"
					+ " FROM usuario"
					+ " WHERE name='"+ventana.getUsuarioLogueado().getNombre()+"'");
					
			if (resUsuarioData.next()) {
				numeroPartidas=resUsuarioData.getInt("nMatches");
				numeroVictorias=resUsuarioData.getInt("nWins");
			} 
			
			actualizarStatement.execute("UPDATE usuario"
					+ " SET nMatches = "+(numeroPartidas+1)+""
					+ " WHERE name='"+ventana.getUsuarioLogueado().getNombre()+"'");
			
			if (victoria==true) {
				actualizarStatement.execute("UPDATE usuario"
						+ " SET nWins = "+(numeroVictorias+1)+""
						+ " WHERE name='"+ventana.getUsuarioLogueado().getNombre()+"'");
			}
			
			actualizarStatement.close();
			bd.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Funcion que declara bancarrota al jugador
	 */
	public void declararBancarrota() {
		int optBancarrota=0;
		do {
			optBancarrota=JOptionPane.showOptionDialog(this.getParent(), "Parece ser que su balance es negativo\nElija lo que desea hacer", jugadores[0].getNombre(), JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, new String[] {"Hipotecar", "Bancarrota"}, null);
			if (optBancarrota==-1) {
				JOptionPane.showMessageDialog(this.getParent(), "Por favor, elija una de las opciones");
			}
		} while (optBancarrota==-1);
		if (optBancarrota==0) {
			jugadores[0].funcionHipotecar();
			if (jugadores[0].getDineroBalance()<0) {
				declararBancarrota();
			}
		} else if (optBancarrota==1) {
			String accion="";
			jugadores[0].setEstaBancarrota(true);
			accion=jugadores[0].getNombre()+" ha declarado bancarrota.";
			JOptionPane.showMessageDialog(ventana, accion, "Bancarrota", JOptionPane.INFORMATION_MESSAGE);
			resumenPartido(accion+"\n\nJugador ha salido del partido");
			thisRef.setVisible(false);
			if (ventana.getUsuarioLogueado()!=null) {
				actualizarDatos(false);
			}
			ventana.cargarPantallaInicio();
		}
	}
}
