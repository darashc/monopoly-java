package componentes;

import java.awt.Font;

import javax.swing.JButton;

/**
 * Clase para boton personalizado
 * @author Darashc
 *
 */
public class BotonPropio extends JButton {
	
	/**
	 * Constructor de BotonPropio
	 * @param text Texto que contiene el boton
	 * @param t Tama�o de la letra
	 */
	public BotonPropio (String text, int t) {
		super(text);
		this.setFont(new Font("Corbel", Font.PLAIN, t));;
	}
}
