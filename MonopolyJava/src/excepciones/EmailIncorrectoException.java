package excepciones;

/**
 * Clase para una excepcion personalizada en caso de introducir email err�neo
 * @author Darashc
 *
 */
public class EmailIncorrectoException extends Exception {
	/**
	 * Constructor de la excepcion
	 * @param msg Mensaje de excepcion
	 */
	public EmailIncorrectoException (String msg) {
		super(msg);
	}
}
