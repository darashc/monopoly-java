-- Creación de la base de datos
CREATE SCHEMA IF NOT EXISTS Monopoly;
USE Monopoly;

-- Tablas
CREATE TABLE IF NOT EXISTS `usuario` (
  `name` VARCHAR(20) NOT NULL,
  `email` VARCHAR(40) NOT NULL,
  `password` VARCHAR(20) NOT NULL,
  `nMatches` INT NOT NULL,
  `nWins` INT NOT NULL,
  `lastConnection` VARCHAR(40) NOT NULL,
  PRIMARY KEY (`name`, `email`, `password`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `suerte` (
  `id` INT NOT NULL,
  `descripcion` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `arca_comunal` (
  `id` INT NOT NULL,
  `descripcion` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `propiedades` (
  `posicion` INT NOT NULL,
  `nombre` VARCHAR(40) NOT NULL,
  `valor` INT NOT NULL,
  `precioAlquiler` INT NOT NULL,
  `precioHipoteca` INT NOT NULL,
  `precioRedimir` INT NOT NULL,
  `precioConstruir` INT,
  `precioVender` INT,
  `precioCasa1` INT,
  `precioCasas2` INT,
  `precioCasas3` INT,
  `precioCasas4` INT,
  `precioHotel` INT,
  PRIMARY KEY (`posicion`, `nombre`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `estaciones` (
  `posicion` INT NOT NULL,
  `nombre` VARCHAR(40) NOT NULL,
  `valor` INT NOT NULL,
  `precioAlquiler1` INT NOT NULL,
  `precioAlquiler2` INT NOT NULL,
  `precioAlquiler3` INT NOT NULL,
  `precioAlquiler4` INT NOT NULL,
  `precioHipoteca` INT NOT NULL,
  `precioRedimir` INT NOT NULL,
  PRIMARY KEY (`posicion`, `nombre`))
ENGINE = InnoDB;

INSERT INTO `suerte` VALUES (1, 'Avance a ISLAS CANARIAS.');
INSERT INTO `suerte` VALUES (2, 'Viaje estación MÁLAGA MARIA ZAMBRANO, si pasa por "GO" cobra 200€');
INSERT INTO `suerte` VALUES (3, 'Por cumplimiento en pago del préstamo de construcción cobre 150€.');
INSERT INTO `suerte` VALUES (4, 'Avance a VALENCIA. Si pasa por "GO" cobre 200€.');
INSERT INTO `suerte` VALUES (5, 'Váyase a la CÁRCEL. No pase por "GO", no cobra 200€.');
INSERT INTO `suerte` VALUES (6, 'Avance a "GO", cobre 200€.');
INSERT INTO `suerte` VALUES (7, 'El banco le paga dividendos por 50€.');
INSERT INTO `suerte` VALUES (8, 'SALGA DE LA CARCEL GRATIS');
INSERT INTO `suerte` VALUES (9, 'Avance a ZARAGOZA. Si pasa por "GO" cobre 200€.');
INSERT INTO `suerte` VALUES (10, 'Retroceda 3 casillas');
INSERT INTO `suerte` VALUES (11, 'Pague multa por exceso de velocidad. 15€.');


INSERT INTO `arca_comunal` VALUES (1, 'SALGA DE LA CÁRCEL GRATIS');
INSERT INTO `arca_comunal` VALUES (2, 'Honorarios médicos, pague 50€');
INSERT INTO `arca_comunal` VALUES (3, 'Vencimiento de fondo vacacional, reciba 100€.');
INSERT INTO `arca_comunal` VALUES (4, 'Vencimiento de seguro de vida. Cobre 100€.');
INSERT INTO `arca_comunal` VALUES (5, 'Pague colegiaturas por 50€.');
INSERT INTO `arca_comunal` VALUES (6, 'Devolución de impuestos. Cobre 20€.');
INSERT INTO `arca_comunal` VALUES (7, 'Pague cuenta de hospital por 100€');
INSERT INTO `arca_comunal` VALUES (8, 'Váyase a la CÁRCEL. No pase por "GO", no cobra 200€.');
INSERT INTO `arca_comunal` VALUES (9, 'Reciba 25€ por su consultoria.');
INSERT INTO `arca_comunal` VALUES (10, 'Herede 100€');
INSERT INTO `arca_comunal` VALUES (11, 'Por venta de acciones, reciba 50€');
INSERT INTO `arca_comunal` VALUES (12, 'Consiguió 2º lugar en un concurso de belleza. Cobre 10€');
INSERT INTO `arca_comunal` VALUES (13, 'Avance a "GO". Cobre 200€');
INSERT INTO `arca_comunal` VALUES (14, 'Error bancario a su favor. Cobre 200€');


INSERT INTO `propiedades` VALUES (1, 'Cáceres', 100, 6, 50, 110, 50, 30, 30, 90, 270, 400, 550);
INSERT INTO `propiedades` VALUES (3, 'Badajoz', 120, 8, 60, 125, 50, 30, 40, 100, 300, 450, 600);
INSERT INTO `propiedades` VALUES (6, 'Granada', 300, 26, 150, 330, 200, 125, 130, 390, 900, 1100, 1275);
INSERT INTO `propiedades` VALUES (7, 'Córdoba', 300, 26, 150, 330, 200, 125, 130, 390, 900, 1100, 1275);
INSERT INTO `propiedades` VALUES (9, 'Sevilla', 320, 28, 160, 350, 200, 125, 150, 450, 1000, 1200, 1400);
INSERT INTO `propiedades` VALUES (11, 'Alicante', 180, 14, 90, 200, 100, 70, 70, 200, 550, 700, 900);
INSERT INTO `propiedades` VALUES (12, 'Castellón', 180, 14, 90, 200, 100, 70, 70, 200, 550, 700, 900);
INSERT INTO `propiedades` VALUES (13, 'Valencia', 200, 16, 100, 220, 100, 70, 80, 220, 600, 800, 1000);
INSERT INTO `propiedades` (posicion, nombre, valor, precioAlquiler, precioHipoteca, precioRedimir) VALUES (14, 'Universidad', 180, 25, 90, 200);
INSERT INTO `propiedades` VALUES (17, "Lleida", 260, 22, 130, 285, 150, 100, 110, 330, 800, 975, 1150);
INSERT INTO `propiedades` VALUES (18, "Tarragona", 260, 22, 285, 85, 150, 100, 110, 330, 800, 975, 1150);
INSERT INTO `propiedades` VALUES (19, "Girona", 280, 24, 140, 310, 150, 100, 120, 360, 850, 1025, 1200);
INSERT INTO `propiedades` VALUES (21, "Huesca", 140, 10, 70, 155, 100, 70, 50, 150, 450, 625, 750);
INSERT INTO `propiedades` VALUES (22, "Teruel", 140, 10, 70, 155, 100, 70, 50, 150, 450, 625, 750);
INSERT INTO `propiedades` VALUES (24, "Zaragoza", 160, 12, 175, 50, 100, 70, 60, 180, 500, 700, 900);
INSERT INTO `propiedades` (posicion, nombre, valor, precioAlquiler, precioHipoteca, precioRedimir) VALUES (26, 'Compañia Solar', 180, 25, 90, 200);
INSERT INTO `propiedades` VALUES (27, 'Lugo', 160, 12, 80, 175, 100, 70, 60, 180, 500, 700, 900);
INSERT INTO `propiedades` VALUES (28, 'Vigo', 160, 12, 80, 175, 100, 70, 60, 180, 500, 700, 900);
INSERT INTO `propiedades` VALUES (29, 'Orense', 180, 14, 90, 200, 100, 70, 70, 200, 550, 700, 900);
INSERT INTO `propiedades` VALUES (31, 'Guadalajara', 240, 20, 120, 265, 150, 100, 100, 300, 750, 925, 110);
INSERT INTO `propiedades` VALUES (32, 'Cuenca', 240, 20, 120, 80, 265, 100, 100, 300, 750, 925, 110);
INSERT INTO `propiedades` VALUES (33, 'Toledo', 260, 22, 130, 85, 285, 100, 110, 330, 800, 975, 1150);
INSERT INTO `propiedades` VALUES (37, 'Islas Baleares', 350, 35, 175, 385, 200, 125, 175, 500, 1100, 1300, 1500);
INSERT INTO `propiedades` VALUES (39, 'Islas Canarias', 400, 50, 200, 440, 200, 125, 200, 600, 1400, 1700, 2000);

INSERT INTO `estaciones` VALUES (5, 'Málaga María Zambrano', 200, 25, 50, 100, 200, 100, 220);
INSERT INTO `estaciones` VALUES (15, 'Barcelona Sants', 200, 25, 50, 100, 200, 100, 220);
INSERT INTO `estaciones` VALUES (25, 'Santiago de Compostela', 200, 25, 50, 100, 200, 100, 220);
INSERT INTO `estaciones` VALUES (34, 'Madrid Atocha', 200, 25, 50, 100, 200, 100, 220);

/* SELECT *
FROM usuario; 

SELECT *
FROM suerte;

SELECT *
FROM arca_comunal;

SELECT *
FROM propiedades;

SELECT *
FROM estaciones; */